import request from '@/utils/request'

// 登录方法
export function upload(data, code, uuid) {
     // let self = this
     //  let file = e.target.files[0]
     //  /* eslint-disable no-undef */
     //  let param = new FormData()  // 创建form对象
     //  param.append('file', file)  // 通过append向form对象添加数据
     //  param.append('chunk', '0') // 添加form表单中其他数据
     //  console.log(param.get('file')) // FormData私有类对象，访问不到，可以通过get判断值是否传进去
      let config = {
        headers: {'Content-Type': 'multipart/form-data'}
      }
     // 添加请求头
    // axios.post('http://172.19.26.60:8081/rest/user/headurl', data, config)
    //     .then(res => {
    //       if (res.data.code === 0) {
    //         self.ImgUrl = res.data.data
    //       }
    //       console.log(res.data)
    //     })
    // }
  return request({
    url: '/common/upload',
    method: 'post',
    data,
    config
  })
}