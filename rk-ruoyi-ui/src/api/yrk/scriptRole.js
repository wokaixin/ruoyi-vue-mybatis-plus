import request from '@/utils/request'

// 查询剧本角色列表
export function listScriptRole(query) {
  return request({
    url: '/yrk/scriptRole/list',
    method: 'get',
    params: query
  })
}

// 查询剧本角色详细
export function getScriptRole(id) {
  return request({
    url: '/yrk/scriptRole/' + id,
    method: 'get'
  })
}

// 新增剧本角色
export function addScriptRole(data) {
  return request({
    url: '/yrk/scriptRole',
    method: 'post',
    data: data
  })
}

// 修改剧本角色
export function updateScriptRole(data) {
  return request({
    url: '/yrk/scriptRole',
    method: 'put',
    data: data
  })
}

// 删除剧本角色
export function delScriptRole(id) {
  return request({
    url: '/yrk/scriptRole/' + id,
    method: 'delete'
  })
}
