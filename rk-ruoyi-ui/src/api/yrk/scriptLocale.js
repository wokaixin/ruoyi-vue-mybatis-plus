import request from '@/utils/request'

// 查询剧本场景列表
export function listScriptLocale(query) {
  return request({
    url: '/yrk/scriptLocale/list',
    method: 'get',
    params: query
  })
}

// 查询剧本场景详细
export function getScriptLocale(id) {
  return request({
    url: '/yrk/scriptLocale/' + id,
    method: 'get'
  })
}

// 新增剧本场景
export function addScriptLocale(data) {
  return request({
    url: '/yrk/scriptLocale',
    method: 'post',
    data: data
  })
}

// 修改剧本场景
export function updateScriptLocale(data) {
  return request({
    url: '/yrk/scriptLocale',
    method: 'put',
    data: data
  })
}

// 删除剧本场景
export function delScriptLocale(id) {
  return request({
    url: '/yrk/scriptLocale/' + id,
    method: 'delete'
  })
}
