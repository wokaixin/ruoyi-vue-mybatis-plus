import request from '@/utils/request'

// 查询剧本道具列表
export function listScriptProp(query) {
  return request({
    url: '/yrk/scriptProp/list',
    method: 'get',
    params: query
  })
}

// 查询剧本道具详细
export function getScriptProp(id) {
  return request({
    url: '/yrk/scriptProp/' + id,
    method: 'get'
  })
}

// 新增剧本道具
export function addScriptProp(data) {
  return request({
    url: '/yrk/scriptProp',
    method: 'post',
    data: data
  })
}

// 修改剧本道具
export function updateScriptProp(data) {
  return request({
    url: '/yrk/scriptProp',
    method: 'put',
    data: data
  })
}

// 删除剧本道具
export function delScriptProp(id) {
  return request({
    url: '/yrk/scriptProp/' + id,
    method: 'delete'
  })
}
