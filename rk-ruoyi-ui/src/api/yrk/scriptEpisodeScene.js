import request from '@/utils/request'

// 查询分场剧情列表
export function listScriptEpisodeScene(query) {
  return request({
    url: '/yrk/scriptEpisodeScene/list',
    method: 'get',
    params: query
  })
}

// 导出
export function exportScriptLocale(data) {
  return request({
    url: '/yrk/scriptEpisodeScene/export',
    method: 'post',
    data: data
  })
}
// 查询分场剧情详细
export function getScriptEpisodeScene(id) {
  return request({
    url: '/yrk/scriptEpisodeScene/' + id,
    method: 'get'
  })
}

// 新增分场剧情
export function addScriptEpisodeScene(data) {
  return request({
    url: '/yrk/scriptEpisodeScene',
    method: 'post',
    data: data
  })
}

// 修改分场剧情
export function updateScriptEpisodeScene(data) {
  return request({
    url: '/yrk/scriptEpisodeScene',
    method: 'put',
    data: data
  })
}

// 删除分场剧情
export function delScriptEpisodeScene(id) {
  return request({
    url: '/yrk/scriptEpisodeScene/' + id,
    method: 'delete'
  })
}
