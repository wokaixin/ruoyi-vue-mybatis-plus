import request from '@/utils/request'

// 查询分集剧情列表
export function listScriptEpisode(query) {
  return request({
    url: '/yrk/scriptEpisode/list',
    method: 'get',
    params: query
  })
}
// 导出
export function exportScriptLocale(data) {
  return request({
    url: '/yrk/scriptEpisode/export',
    method: 'post',
    data: data
  })
}
// 查询分集剧情详细
export function getScriptEpisode(id) {
  return request({
    url: '/yrk/scriptEpisode/' + id,
    method: 'get'
  })
}

// 新增分集剧情
export function addScriptEpisode(data) {
  return request({
    url: '/yrk/scriptEpisode',
    method: 'post',
    data: data
  })
}

// 修改分集剧情
export function updateScriptEpisode(data) {
  return request({
    url: '/yrk/scriptEpisode',
    method: 'put',
    data: data
  })
}

// 删除分集剧情
export function delScriptEpisode(id) {
  return request({
    url: '/yrk/scriptEpisode/' + id,
    method: 'delete'
  })
}
