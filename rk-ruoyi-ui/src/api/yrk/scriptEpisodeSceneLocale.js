import request from '@/utils/request'

// 查询剧本分场剧情关联地点列表
export function listScriptEpisodeSceneLocale(query) {
  return request({
    url: '/yrk/scriptEpisodeSceneLocale/list',
    method: 'get',
    params: query
  })
}

// 查询剧本分场剧情关联地点详细
export function getScriptEpisodeSceneLocale(sceneId) {
  return request({
    url: '/yrk/scriptEpisodeSceneLocale/' + sceneId,
    method: 'get'
  })
}

// 新增剧本分场剧情关联地点
export function addScriptEpisodeSceneLocale(data) {
  return request({
    url: '/yrk/scriptEpisodeSceneLocale',
    method: 'post',
    data: data
  })
}

// 修改剧本分场剧情关联地点
export function updateScriptEpisodeSceneLocale(data) {
  return request({
    url: '/yrk/scriptEpisodeSceneLocale',
    method: 'put',
    data: data
  })
}

// 删除剧本分场剧情关联地点
export function delScriptEpisodeSceneLocale(sceneId) {
  return request({
    url: '/yrk/scriptEpisodeSceneLocale/' + sceneId,
    method: 'delete'
  })
}
