import request from '@/utils/request'

// 查询剧本分场剧情关联道具列表
export function listScriptEpisodeSceneProp(query) {
  return request({
    url: '/yrk/scriptEpisodeSceneProp/list',
    method: 'get',
    params: query
  })
}

// 查询剧本分场剧情关联道具详细
export function getScriptEpisodeSceneProp(sceneId) {
  return request({
    url: '/yrk/scriptEpisodeSceneProp/' + sceneId,
    method: 'get'
  })
}

// 新增剧本分场剧情关联道具
export function addScriptEpisodeSceneProp(data) {
  return request({
    url: '/yrk/scriptEpisodeSceneProp',
    method: 'post',
    data: data
  })
}

// 修改剧本分场剧情关联道具
export function updateScriptEpisodeSceneProp(data) {
  return request({
    url: '/yrk/scriptEpisodeSceneProp',
    method: 'put',
    data: data
  })
}

// 删除剧本分场剧情关联道具
export function delScriptEpisodeSceneProp(sceneId) {
  return request({
    url: '/yrk/scriptEpisodeSceneProp/' + sceneId,
    method: 'delete'
  })
}
