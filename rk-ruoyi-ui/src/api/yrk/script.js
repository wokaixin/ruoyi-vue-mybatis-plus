import request from '@/utils/request'

// 查询剧本基础信息列表
export function listScript(query) {
  return request({
    url: '/yrk/script/list',
    method: 'get',
    params: query
  })
}

// 查询剧本基础信息详细
export function getScript(id) {
  return request({
    url: '/yrk/script/' + id,
    method: 'get'
  })
}

// 新增剧本基础信息
export function addScript(data) {
  return request({
    url: '/yrk/script',
    method: 'post',
    data: data
  })
}

// 修改剧本基础信息
export function updateScript(data) {
  return request({
    url: '/yrk/script',
    method: 'put',
    data: data
  })
}

// 删除剧本基础信息
export function delScript(id) {
  return request({
    url: '/yrk/script/' + id,
    method: 'delete'
  })
}
// 导出
export function exportScript(data) {
  return request({
    url: '/yrk/script/export',
    method: 'post',
    data: data
  })
}
