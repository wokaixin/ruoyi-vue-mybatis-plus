import request from '@/utils/request'

// 查询剧本分场剧情关联角色列表
export function listScriptEpisodeSceneRole(query) {
  return request({
    url: '/yrk/scriptEpisodeSceneRole/list',
    method: 'get',
    params: query
  })
}

// 查询剧本分场剧情关联角色详细
export function getScriptEpisodeSceneRole(sceneId) {
  return request({
    url: '/yrk/scriptEpisodeSceneRole/' + sceneId,
    method: 'get'
  })
}

// 新增剧本分场剧情关联角色
export function addScriptEpisodeSceneRole(data) {
  return request({
    url: '/yrk/scriptEpisodeSceneRole',
    method: 'post',
    data: data
  })
}

// 修改剧本分场剧情关联角色
export function updateScriptEpisodeSceneRole(data) {
  return request({
    url: '/yrk/scriptEpisodeSceneRole',
    method: 'put',
    data: data
  })
}

// 删除剧本分场剧情关联角色
export function delScriptEpisodeSceneRole(sceneId) {
  return request({
    url: '/yrk/scriptEpisodeSceneRole/' + sceneId,
    method: 'delete'
  })
}
