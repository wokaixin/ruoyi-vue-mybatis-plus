import request from '@/utils/request'

// 查询话题分类列表
export function listTopic_cate(query) {
  return request({
    url: '/topic_cate/list',
    method: 'get',
    params: query
  })
}

// 查询话题分类详细
export function getTopic_cate(id) {
  return request({
    url: '/topic_cate/' + id,
    method: 'get'
  })
}

// 新增话题分类
export function addTopic_cate(data) {
  return request({
    url: '/topic_cate',
    method: 'post',
    data: data
  })
}

// 修改话题分类
export function updateTopic_cate(data) {
  return request({
    url: '/topic_cate',
    method: 'put',
    data: data
  })
}

// 删除话题分类
export function delTopic_cate(id) {
  return request({
    url: '/topic_cate/' + id,
    method: 'delete'
  })
}
