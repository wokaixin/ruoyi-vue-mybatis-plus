import request from '@/utils/request'

// 查询圈子分类列表
export function listCategory(query) {
  return request({
    url: '/category/list',
    method: 'get',
    params: query
  })
}

// 查询圈子分类详细
export function getCategory(cateId) {
  return request({
    url: '/category/' + cateId,
    method: 'get'
  })
}

// 新增圈子分类
export function addCategory(data) {
  return request({
    url: '/category',
    method: 'post',
    data: data
  })
}

// 修改圈子分类
export function updateCategory(data) {
  return request({
    url: '/category',
    method: 'put',
    data: data
  })
}

// 删除圈子分类
export function delCategory(cateId) {
  return request({
    url: '/category/' + cateId,
    method: 'delete'
  })
}
