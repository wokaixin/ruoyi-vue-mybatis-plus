package com.ruoyi.yrk.system.controller;
import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.yrk.system.domain.Script;
import com.ruoyi.yrk.system.dto.ScriptDto;
import com.ruoyi.yrk.system.service.IScriptService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;


/**
 * 剧本基础信息Controller
 *
 * @author yichen
 * @date 2021-11-21
 */
@RestController
@RequestMapping("/yrk/script")
public class ScriptController extends BaseController
{
    @Autowired
    private IScriptService yrkScriptService;

/**
 * 查询剧本基础信息列表
 */
@PreAuthorize("@ss.hasPermi('yrk:script:list')")
@GetMapping("/list")
        public TableDataInfo list(ScriptDto yrkScript)
    {
        startPage();
        List<Script> list = yrkScriptService.selectdList(yrkScript);
        return getDataTable(list);
    }
    
    /**
     * 导出剧本基础信息列表
     */
    @PreAuthorize("@ss.hasPermi('yrk:script:export')")
    @Log(title = "剧本基础信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ScriptDto yrkScript) throws IOException
    {

        startPage();
        List<Script> list = yrkScriptService.selectdList(yrkScript);
        ExcelUtil<Script> util = new ExcelUtil<Script>(Script.class);
        util.exportExcel(response, list, "剧本基础信息数据");
    }
    /**
     * 导入剧本基础信息
     */
    @PreAuthorize("@ss.hasPermi('yrk:script:importData')")
    @Log(title = "剧本基础信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<Script> util = new ExcelUtil< Script>( Script.class);
        List<Script> list = util.importExcel(file.getInputStream());
        boolean message=false;
        if(updateSupport){
            message =yrkScriptService.saveOrUpdateBatch(list);
        }else {
            message =yrkScriptService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }
    /**
     * 获取剧本基础信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('yrk:script:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(yrkScriptService.getById(id));
    }

    /**
     * 新增剧本基础信息
     */
    @PreAuthorize("@ss.hasPermi('yrk:script:add')")
    @Log(title = "剧本基础信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Script yrkScript)
    {

        yrkScript.setUserId(SecurityUtils.getUserId());
        return toAjax(yrkScriptService.save(yrkScript));
    }

    /**
     * 修改剧本基础信息
     */
    @PreAuthorize("@ss.hasPermi('yrk:script:edit')")
    @Log(title = "剧本基础信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Script yrkScript)
    {
        yrkScript.setUserId(SecurityUtils.getUserId());
        return toAjax(yrkScriptService.updateById(yrkScript));
    }

    /**
     * 删除剧本基础信息
     */
    @PreAuthorize("@ss.hasPermi('yrk:script:remove')")
    @Log(title = "剧本基础信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids)
    {
        return toAjax(yrkScriptService.removeByIds(ids));
    }
}
