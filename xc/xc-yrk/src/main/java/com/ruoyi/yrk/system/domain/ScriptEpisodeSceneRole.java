package com.ruoyi.yrk.system.domain;

import lombok.Data;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.domain.XBaseEntity;

/**
 * 剧本分场剧情关联角色对象 yrk_script_episode_scene_role
 * 
 * @author yichen
 * @date 2021-11-21
 */
@Data
public class ScriptEpisodeSceneRole extends  XBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 分场id */
    @Excel(name = "分场id")
    private Long sceneId;
    /** 角色id */
    @Excel(name = "角色id")
    private Long roleId;
}
