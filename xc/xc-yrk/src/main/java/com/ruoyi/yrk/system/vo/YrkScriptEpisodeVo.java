package com.ruoyi.yrk.system.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.vo.XVoBaseEntity;

/**
 * 分集剧情对象 yrk_script_episode
 * 
 * @author yichen
 * @date 2021-11-21
 */
@Data
public class YrkScriptEpisodeVo extends  XVoBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** id */
    private Long id;
    /** 集数 */
    @Excel(name = "集数")
    private Long number;
    /** 标题 */
    @Excel(name = "标题")
    private String title;
    /** 描述 */
    @Excel(name = "描述")
    private String describes;
    /** 剧本id */
    @Excel(name = "剧本id")
    private Long scriptId;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    /** 状态 */
    @Excel(name = "状态")
    private Long status;
    /** 内容 */
    @Excel(name = "内容")
    private String content;
    /** 备注 */
    @Excel(name = "备注")
    private String notes;
    /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deleteTime;
}
