package com.ruoyi.yrk.system.controller;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.yrk.system.domain.ScriptProp;
import com.ruoyi.yrk.system.dto.ScriptPropDto;
import com.ruoyi.yrk.system.service.IScriptPropService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;


/**
 * 剧本道具Controller
 *
 * @author yichen
 * @date 2021-11-21
 */
@RestController
@RequestMapping("/yrk/scriptProp")
public class ScriptPropController extends BaseController
{
    @Autowired
    private IScriptPropService yrkScriptPropService;

/**
 * 查询剧本道具列表
 */
@PreAuthorize("@ss.hasPermi('yrk:scriptProp:list')")
@GetMapping("/list")
        public TableDataInfo list(ScriptPropDto yrkScriptProp)
    {
        startPage();
        List<ScriptProp> list = yrkScriptPropService.selectdList(yrkScriptProp);
        return getDataTable(list);
    }
    
    /**
     * 导出剧本道具列表
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptProp:export')")
    @Log(title = "剧本道具", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ScriptPropDto yrkScriptProp) throws IOException
    {

        startPage();
        List<ScriptProp> list = yrkScriptPropService.selectdList(yrkScriptProp);
        ExcelUtil<ScriptProp> util = new ExcelUtil<ScriptProp>(ScriptProp.class);
        util.exportExcel(response, list, "剧本道具数据");
    }
    /**
     * 导入剧本道具
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptProp:importData')")
    @Log(title = "剧本道具导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<ScriptProp> util = new ExcelUtil<ScriptProp>( ScriptProp.class);
        List<ScriptProp> list = util.importExcel(file.getInputStream());
        boolean message=false;
        if(updateSupport){
            message =yrkScriptPropService.saveOrUpdateBatch(list);
        }else {
            message =yrkScriptPropService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }
    /**
     * 获取剧本道具详细信息
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptProp:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(yrkScriptPropService.getById(id));
    }

    /**
     * 新增剧本道具
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptProp:add')")
    @Log(title = "剧本道具", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ScriptProp yrkScriptProp)
    {
        return toAjax(yrkScriptPropService.save(yrkScriptProp));
    }

    /**
     * 修改剧本道具
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptProp:edit')")
    @Log(title = "剧本道具", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ScriptProp yrkScriptProp)
    {
        return toAjax(yrkScriptPropService.updateById(yrkScriptProp));
    }

    /**
     * 删除剧本道具
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptProp:remove')")
    @Log(title = "剧本道具", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids)
    {
        return toAjax(yrkScriptPropService.removeByIds(ids));
    }
}
