package com.ruoyi.yrk.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.yrk.system.domain.ScriptProp;
import com.ruoyi.yrk.system.dto.ScriptPropDto;
/**
 * 剧本道具Service接口
 * 
 * @author yichen
 * @date 2021-11-21
 */
public interface IScriptPropService extends IService<ScriptProp>
{


    /**
     * 查询剧本道具列表
     * 
     * @param yrkScriptProp 剧本道具
     * @return 剧本道具集合
     */
    public List<ScriptProp> selectdList(ScriptPropDto yrkScriptProp);

}
