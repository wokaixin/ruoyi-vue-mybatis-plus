package com.ruoyi.yrk.system.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.yrk.system.mapper.ScriptEpisodeScenePropMapper;
import com.ruoyi.yrk.system.domain.ScriptEpisodeSceneProp;
import com.ruoyi.yrk.system.dto.ScriptEpisodeScenePropDto;
import com.ruoyi.yrk.system.service.IScriptEpisodeScenePropService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 * 剧本分场剧情关联道具Service业务层处理
 *
 * @author yichen
 * @date 2021-11-21
 */
@Service
<<<<<<< HEAD:xc/xc-yrk/src/main/java/com/ruoyi/yrk/system/service/impl/ScriptEpisodeScenePropServiceImpl.java
public class ScriptEpisodeScenePropServiceImpl extends ServiceImpl<ScriptEpisodeScenePropMapper, ScriptEpisodeSceneProp> implements IScriptEpisodeScenePropService {
    @Autowired
    private ScriptEpisodeScenePropMapper yrkScriptEpisodeScenePropMapper;
=======
public class YrkScriptEpisodeScenePropServiceImpl extends ServiceImpl<YrkScriptEpisodeScenePropMapper, YrkScriptEpisodeSceneProp> implements IYrkScriptEpisodeScenePropService {
>>>>>>> 49dc978e45d24fe85f98d4de0c1313c812f25ad4:xingchen/xingchen-yrk/src/main/java/com/ruoyi/yrk/system/service/impl/YrkScriptEpisodeScenePropServiceImpl.java

    /**
     * 查询剧本分场剧情关联道具列表
     * 
     * @param yrkScriptEpisodeSceneProp 剧本分场剧情关联道具
     * @return 剧本分场剧情关联道具
     */
    @Override
    public List<ScriptEpisodeSceneProp> selectdList(ScriptEpisodeScenePropDto yrkScriptEpisodeSceneProp)
    {
        QueryWrapper<ScriptEpisodeSceneProp> q= new QueryWrapper<>();
        if(yrkScriptEpisodeSceneProp.getSceneId()!=null  ){
            q.eq("scene_id",yrkScriptEpisodeSceneProp.getSceneId());
        }
        if(yrkScriptEpisodeSceneProp.getPropId()!=null  ){
            q.eq("prop_id",yrkScriptEpisodeSceneProp.getPropId());
        }
        List<ScriptEpisodeSceneProp> list = this.list(q);
        return list;
    }


}
