package com.ruoyi.yrk.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.yrk.system.domain.ScriptLocale;
import com.ruoyi.yrk.system.dto.ScriptLocaleDto;
/**
 * 剧本场景Service接口
 * 
 * @author yichen
 * @date 2021-11-21
 */
public interface IScriptLocaleService extends IService<ScriptLocale>
{


    /**
     * 查询剧本场景列表
     * 
     * @param yrkScriptLocale 剧本场景
     * @return 剧本场景集合
     */
    public List<ScriptLocale> selectdList(ScriptLocaleDto yrkScriptLocale);

}
