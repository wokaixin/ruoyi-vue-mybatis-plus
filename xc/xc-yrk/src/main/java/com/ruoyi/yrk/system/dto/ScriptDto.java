package com.ruoyi.yrk.system.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.dto.XDtoBaseEntity;

/**
 * 剧本基础信息对象 yrk_script
 * 
 * @author yichen
 * @date 2021-11-21
 */
@Data
public class ScriptDto extends  XDtoBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** id */
    private Long id;
            /** 剧本名称 */
    private String name;
            /** 用户id */
    private Long userId;
    /** 描述 */
    @Excel(name = "描述")
    private String describes;
            /** 作者 */
    private String author;
            /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginCreateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endCreateTime;
        /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginUpdateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endUpdateTime;
        /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginDeleteTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDeleteTime;
        /** 状态 */
    private Integer status;
        


}
