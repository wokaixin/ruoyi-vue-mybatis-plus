package com.ruoyi.yrk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.yrk.system.domain.YrkScriptEpisode;
import com.ruoyi.yrk.system.dto.YrkScriptEpisodeDto;
import com.ruoyi.yrk.system.dto.YrkScriptEpisodePathSceneDto;

import java.util.List;

/**
 * 分集剧情Service接口
 * 
 * @author yichen
 * @date 2021-11-21
 */
public interface IYrkScriptEpisodePathService
{

    /**
     * 根据单集剧本内容 爬取分场信息
     * @param episode
     * @return
     */
    public List<YrkScriptEpisodePathSceneDto> pathSceneInfo(String episode);
    /**
     * 抓取 剧本分场详情 包含属性信息 例如 角色：杀手 道具：倚天剑
     * @param episode
     * @param isAttributes
     * @return
     */
    public List<YrkScriptEpisodePathSceneDto> pathSceneInfo(String episode,Boolean isAttributes);
    /**
     * 抓取 剧本分场详情 包含属性信息 例如 角色：杀手 道具：倚天剑
     *
     * @param episode
     * @param attributes 属性集合 例如: [角色,道具]
     * @return
     */
    public List<YrkScriptEpisodePathSceneDto> pathSceneInfo(String episode,List<String> attributes);
    /**
     *  剧本场次摘出属性 例如 角色：杀手，群众，
     * @param attribute
     * @param episode
     * @return
     */
    public String  pathAttribute(String attribute,String episode);
    /**
     * 剧本场次摘出属性 例如 角色：杀手，群众，
     *
     * @param attribute
     * @param episode
     * @paramdoEnd 是否匹配到结尾 否则只匹配到行
     * @return
     */
    public String pathAttribute(String attribute, String episode,Boolean doEnd);
}
