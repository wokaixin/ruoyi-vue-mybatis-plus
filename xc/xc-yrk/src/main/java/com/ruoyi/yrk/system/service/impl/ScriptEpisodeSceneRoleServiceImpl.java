package com.ruoyi.yrk.system.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.yrk.system.mapper.ScriptEpisodeSceneRoleMapper;
import com.ruoyi.yrk.system.domain.ScriptEpisodeSceneRole;
import com.ruoyi.yrk.system.dto.ScriptEpisodeSceneRoleDto;
import com.ruoyi.yrk.system.service.IScriptEpisodeSceneRoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 * 剧本分场剧情关联角色Service业务层处理
 *
 * @author yichen
 * @date 2021-11-21
 */
@Service
<<<<<<< HEAD:xc/xc-yrk/src/main/java/com/ruoyi/yrk/system/service/impl/ScriptEpisodeSceneRoleServiceImpl.java
public class ScriptEpisodeSceneRoleServiceImpl extends ServiceImpl<ScriptEpisodeSceneRoleMapper, ScriptEpisodeSceneRole> implements IScriptEpisodeSceneRoleService {
    @Autowired
    private ScriptEpisodeSceneRoleMapper yrkScriptEpisodeSceneRoleMapper;
=======
public class YrkScriptEpisodeSceneRoleServiceImpl extends ServiceImpl<YrkScriptEpisodeSceneRoleMapper, YrkScriptEpisodeSceneRole> implements IYrkScriptEpisodeSceneRoleService {

>>>>>>> 49dc978e45d24fe85f98d4de0c1313c812f25ad4:xingchen/xingchen-yrk/src/main/java/com/ruoyi/yrk/system/service/impl/YrkScriptEpisodeSceneRoleServiceImpl.java

    /**
     * 查询剧本分场剧情关联角色列表
     * 
     * @param yrkScriptEpisodeSceneRole 剧本分场剧情关联角色
     * @return 剧本分场剧情关联角色
     */
    @Override
    public List<ScriptEpisodeSceneRole> selectdList(ScriptEpisodeSceneRoleDto yrkScriptEpisodeSceneRole)
    {
        QueryWrapper<ScriptEpisodeSceneRole> q= new QueryWrapper<>();
        if(yrkScriptEpisodeSceneRole.getSceneId()!=null  ){
            q.eq("scene_id",yrkScriptEpisodeSceneRole.getSceneId());
        }
        if(yrkScriptEpisodeSceneRole.getRoleId()!=null  ){
            q.eq("role_id",yrkScriptEpisodeSceneRole.getRoleId());
        }
        List<ScriptEpisodeSceneRole> list = this.list(q);
        return list;
    }


}
