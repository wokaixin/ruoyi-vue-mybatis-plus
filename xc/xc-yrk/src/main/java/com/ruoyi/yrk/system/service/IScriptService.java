package com.ruoyi.yrk.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.yrk.system.domain.Script;
import com.ruoyi.yrk.system.dto.ScriptDto;
/**
 * 剧本基础信息Service接口
 * 
 * @author yichen
 * @date 2021-11-21
 */
public interface IScriptService extends IService<Script>
{


    /**
     * 查询剧本基础信息列表
     * 
     * @param yrkScript 剧本基础信息
     * @return 剧本基础信息集合
     */
    public List<Script> selectdList(ScriptDto yrkScript);

}
