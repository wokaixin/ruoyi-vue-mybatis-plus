package com.ruoyi.yrk.system.controller;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.yrk.system.domain.ScriptRole;
import com.ruoyi.yrk.system.dto.ScriptRoleDto;
import com.ruoyi.yrk.system.service.IScriptRoleService;
import com.ruoyi.common.utils.poi.ExcelUtil;


/**
 * 剧本角色Controller
 *
 * @author yichen
 * @date 2021-11-21
 */
@RestController
@RequestMapping("/yrk/scriptRole")
public class ScriptRoleController extends BaseController
{
    @Autowired
    private IScriptRoleService yrkScriptRoleService;

/**
 * 查询剧本角色列表
 */
@PreAuthorize("@ss.hasPermi('yrk:scriptRole:list')")
@GetMapping("/list")
            public AjaxResult list(ScriptRoleDto yrkScriptRole)
        {
            startPage();
            List<ScriptRole> list = yrkScriptRoleService.selectdList(yrkScriptRole);
            return AjaxResult.success(list);
        }
    
    /**
     * 导出剧本角色列表
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptRole:export')")
    @Log(title = "剧本角色", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ScriptRoleDto yrkScriptRole) throws IOException
    {

        startPage();
        List<ScriptRole> list = yrkScriptRoleService.selectdList(yrkScriptRole);
        ExcelUtil<ScriptRole> util = new ExcelUtil<ScriptRole>(ScriptRole.class);
        util.exportExcel(response, list, "剧本角色数据");
    }
    /**
     * 导入剧本角色
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptRole:importData')")
    @Log(title = "剧本角色导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<ScriptRole> util = new ExcelUtil<ScriptRole>( ScriptRole.class);
        List<ScriptRole> list = util.importExcel(file.getInputStream());
        boolean message=false;
        if(updateSupport){
            message =yrkScriptRoleService.saveOrUpdateBatch(list);
        }else {
            message =yrkScriptRoleService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }
    /**
     * 获取剧本角色详细信息
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptRole:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(yrkScriptRoleService.getById(id));
    }

    /**
     * 新增剧本角色
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptRole:add')")
    @Log(title = "剧本角色", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ScriptRole yrkScriptRole)
    {
        return toAjax(yrkScriptRoleService.save(yrkScriptRole));
    }

    /**
     * 修改剧本角色
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptRole:edit')")
    @Log(title = "剧本角色", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ScriptRole yrkScriptRole)
    {
        return toAjax(yrkScriptRoleService.updateById(yrkScriptRole));
    }

    /**
     * 删除剧本角色
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptRole:remove')")
    @Log(title = "剧本角色", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids)
    {
        return toAjax(yrkScriptRoleService.removeByIds(ids));
    }
}
