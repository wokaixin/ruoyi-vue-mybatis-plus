package com.ruoyi.yrk.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.yrk.system.domain.ScriptEpisodeScene;
import com.ruoyi.yrk.system.dto.ScriptEpisodeSceneDto;
/**
 * 分场剧情Service接口
 * 
 * @author ruoyi
 * @date 2021-11-21
 */
public interface IScriptEpisodeSceneService extends IService<ScriptEpisodeScene>
{


    /**
     * 查询分场剧情列表
     * 
     * @param yrkScriptEpisodeScene 分场剧情
     * @return 分场剧情集合
     */
    public List<ScriptEpisodeScene> selectdList(ScriptEpisodeSceneDto yrkScriptEpisodeScene);


}
