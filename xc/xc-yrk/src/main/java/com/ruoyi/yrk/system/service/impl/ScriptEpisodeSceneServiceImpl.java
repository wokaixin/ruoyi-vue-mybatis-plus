package com.ruoyi.yrk.system.service.impl;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.yrk.system.mapper.ScriptEpisodeSceneMapper;
import com.ruoyi.yrk.system.domain.ScriptEpisodeScene;
import com.ruoyi.yrk.system.dto.ScriptEpisodeSceneDto;
import com.ruoyi.yrk.system.service.IScriptEpisodeSceneService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 * 分场剧情Service业务层处理
 *
 * @author ruoyi
 * @date 2021-11-21
 */
@Service
<<<<<<< HEAD:xc/xc-yrk/src/main/java/com/ruoyi/yrk/system/service/impl/ScriptEpisodeSceneServiceImpl.java
public class ScriptEpisodeSceneServiceImpl extends ServiceImpl<ScriptEpisodeSceneMapper, ScriptEpisodeScene> implements IScriptEpisodeSceneService {
    @Autowired
    private ScriptEpisodeSceneMapper yrkScriptEpisodeSceneMapper;
=======
public class YrkScriptEpisodeSceneServiceImpl extends ServiceImpl<YrkScriptEpisodeSceneMapper, YrkScriptEpisodeScene> implements IYrkScriptEpisodeSceneService {

>>>>>>> 49dc978e45d24fe85f98d4de0c1313c812f25ad4:xingchen/xingchen-yrk/src/main/java/com/ruoyi/yrk/system/service/impl/YrkScriptEpisodeSceneServiceImpl.java

    /**
     * 查询分场剧情列表
     * 
     * @param yrkScriptEpisodeScene 分场剧情
     * @return 分场剧情
     */
    @Override
    public List<ScriptEpisodeScene> selectdList(ScriptEpisodeSceneDto yrkScriptEpisodeScene)
    {
        QueryWrapper<ScriptEpisodeScene> q= new QueryWrapper<>();
        if(yrkScriptEpisodeScene.getId()!=null  ){
            q.eq("id",yrkScriptEpisodeScene.getId());
        }
        if(yrkScriptEpisodeScene.getTitle()!=null   && !yrkScriptEpisodeScene.getTitle().trim().equals("")){
            q.like("title",yrkScriptEpisodeScene.getTitle());
        }
        if(yrkScriptEpisodeScene.getContent()!=null   && !yrkScriptEpisodeScene.getContent().trim().equals("")){
            q.like("content",yrkScriptEpisodeScene.getContent());
        }
        if(yrkScriptEpisodeScene.getBeginCreateTime()!=null && yrkScriptEpisodeScene.getEndCreateTime()!=null){
            q.between("create_time",yrkScriptEpisodeScene.getBeginCreateTime(),yrkScriptEpisodeScene.getEndCreateTime());
        }
        if(yrkScriptEpisodeScene.getBeginUpdateTime()!=null && yrkScriptEpisodeScene.getEndUpdateTime()!=null){
            q.between("update_time",yrkScriptEpisodeScene.getBeginUpdateTime(),yrkScriptEpisodeScene.getEndUpdateTime());
        }
        if(yrkScriptEpisodeScene.getEpisodeId()!=null  ){
            q.eq("episode_id",yrkScriptEpisodeScene.getEpisodeId());
        }
        if(yrkScriptEpisodeScene.getNumber()!=null   && !yrkScriptEpisodeScene.getNumber().trim().equals("")){
            q.like("number",yrkScriptEpisodeScene.getNumber());
        }
        if(yrkScriptEpisodeScene.getBeginDeleteTime()!=null && yrkScriptEpisodeScene.getEndDeleteTime()!=null){
            q.between("delete_time",yrkScriptEpisodeScene.getBeginDeleteTime(),yrkScriptEpisodeScene.getEndDeleteTime());
        }
        List<ScriptEpisodeScene> list = this.list(q);
        return list;
    }

}
