package com.ruoyi.yrk.system.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.yrk.system.mapper.ScriptPropMapper;
import com.ruoyi.yrk.system.domain.ScriptProp;
import com.ruoyi.yrk.system.dto.ScriptPropDto;
import com.ruoyi.yrk.system.service.IScriptPropService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 * 剧本道具Service业务层处理
 *
 * @author yichen
 * @date 2021-11-21
 */
@Service
public class ScriptPropServiceImpl extends ServiceImpl<ScriptPropMapper, ScriptProp> implements IScriptPropService {
    @Autowired
    private ScriptPropMapper yrkScriptPropMapper;

    /**
     * 查询剧本道具列表
     * 
     * @param yrkScriptProp 剧本道具
     * @return 剧本道具
     */
    @Override
    public List<ScriptProp> selectdList(ScriptPropDto yrkScriptProp)
    {
        QueryWrapper<ScriptProp> q= new QueryWrapper<>();
        if(yrkScriptProp.getId()!=null  ){
            q.eq("id",yrkScriptProp.getId());
        }
        if(yrkScriptProp.getName()!=null   && !yrkScriptProp.getName().trim().equals("")){
            q.like("name",yrkScriptProp.getName());
        }
        if(yrkScriptProp.getBeginCreateTime()!=null && yrkScriptProp.getEndCreateTime()!=null){
            q.between("create_time",yrkScriptProp.getBeginCreateTime(),yrkScriptProp.getEndCreateTime());
        }
        if(yrkScriptProp.getBeginUpdateTime()!=null && yrkScriptProp.getEndUpdateTime()!=null){
            q.between("update_time",yrkScriptProp.getBeginUpdateTime(),yrkScriptProp.getEndUpdateTime());
        }
        if(yrkScriptProp.getScriptId()!=null  ){
            q.eq("script_id",yrkScriptProp.getScriptId());
        }
        if(yrkScriptProp.getStatus()!=null  ){
            q.eq("status",yrkScriptProp.getStatus());
        }
        if(yrkScriptProp.getPid()!=null  ){
            q.eq("pid",yrkScriptProp.getPid());
        }
        List<ScriptProp> list = this.list(q);
        return list;
    }


}
