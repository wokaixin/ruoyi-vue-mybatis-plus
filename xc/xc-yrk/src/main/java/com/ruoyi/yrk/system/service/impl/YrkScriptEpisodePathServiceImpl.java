package com.ruoyi.yrk.system.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.StringUtils;
import com.xingchen.common.utils.StrUtils;
import com.ruoyi.yrk.system.dto.YrkScriptEpisodePathSceneDto;
import com.ruoyi.yrk.system.service.IYrkScriptEpisodePathService;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 分集剧情doc文档爬取数据Service业务层处理
 *
 * @author yichen
 * @date 2021-11-21
 */
@Service
public class YrkScriptEpisodePathServiceImpl implements IYrkScriptEpisodePathService {
    /**
     * 抓取 剧本分场详情 包含属性信息 例如 角色：杀手 道具：倚天剑
     *
     * @param episode
     * @param isAttributes
     * @return
     */
    @Override
    public List<YrkScriptEpisodePathSceneDto> pathSceneInfo(String episode, Boolean isAttributes) {
        List<YrkScriptEpisodePathSceneDto> list = pathSceneInfo(episode);
        if (isAttributes) {
            for (YrkScriptEpisodePathSceneDto item : list) {
//                String role= pathAttribute("角色", item.getContent());

//                if(StringUtils.isNotEmpty(role)){
//                    item.setRole(role.split("，"));
//                }
//                String prop=pathAttribute("道具", item.getContent());
//                if(StringUtils.isNotEmpty(prop)){
//                    item.setProp(prop.split("，"));
//                }
//                String locale=pathAttribute("场景", item.getContent());
//                item.setTime(pathAttribute("时间", item.getContent()));
//                if(StringUtils.isNotEmpty(locale)){
//                    item.setLocale(locale.split("，"));
//                }
//                item.setLandscape(pathAttribute("景别", item.getContent()));
//                item.setWeather(pathAttribute("天气", item.getContent()));
//                截取剧情:之后字符串
//                String content1=item.getContent().substring(0, item.getContent().indexOf("剧情："));
//                String contentInfo=pathAttribute("剧情", item.getContent(),true);
//                if(StringUtils.isNotEmpty(contentInfo)){
//                    item.setContent(contentInfo);
//                }
            }
        }

        return list;
    }
    /**
     * 抓取 剧本分场详情 包含属性信息 例如 角色：杀手 道具：倚天剑
     *
     * @param episode
     * @param attributes 属性集合 例如: [角色,道具]
     * @return
     */
    @Override
    public List<YrkScriptEpisodePathSceneDto> pathSceneInfo(String episode,List<String> attributes) {
        List<YrkScriptEpisodePathSceneDto> list = pathSceneInfo(episode);

        for (YrkScriptEpisodePathSceneDto scene : list) {
            Map<String, List<String>> map = new HashMap<>();//创建Map
            for (String attribute:attributes) {
               String item= pathAttribute(attribute, scene.getContent());
                if(StringUtils.isNotEmpty(item)){
                    scene.setContent(StrUtils.trimN(StrUtils.trimT(StrUtils.trimR(scene.getContent().replace(item,"")))));
                    //                删除属性字头，只留值，逗号分割成数组，
                    map.put(attribute, Arrays.asList( item.replace(attribute + "：", "").trim().split("，")));
                }
            }
            scene.setContentSize(scene.getContent().trim().length());
            scene.setAttributes(map);
        }

        return list;
    }
    /**
     * 根据单集剧本内容 爬取分场信息
     *
     * @param episode
     * @return
     */

    @Override
    public List<YrkScriptEpisodePathSceneDto> pathSceneInfo(String episode) {
//        episode=episode.replace("\r\n","\n");
        String attribute = "场次";
        episode = StrUtils.toEnglish(episode, false);
//            替换中文字符
        String regex = attribute + "：.*[\\r|\\n]";
        Pattern p = Pattern.compile(regex);
        Matcher matcher = p.matcher(episode);
//        matcher.matches();
////          匹配所有符合的 替换
//            System.out.println(str.replace(":","："));
        List<YrkScriptEpisodePathSceneDto> list = new ArrayList();
        int lastEnd = 0;
        int i = 0;
        while (matcher.find()) {

            //每一个符合正则的字符串
            String row = matcher.group().trim();
            //截取出括号中的内容
//                String substring = e.substring(3, e.length()-1);
            if (row.length() > 3) {
                YrkScriptEpisodePathSceneDto yrkScriptEpisodePathSceneDto = new YrkScriptEpisodePathSceneDto();
                yrkScriptEpisodePathSceneDto.setTitle(StrUtils.delHuanhangAndKong(row).replace(attribute + "：", ""));
//                    第二段头才是第一段内容结尾
                if (i > 0) {
                    String content = episode.substring(lastEnd + 1, matcher.start(0));
                    list.get(i - 1).setContent(content.trim());
                    list.get(list.size() - 1).setContentEnd(matcher.start(0)-1);
                }

                yrkScriptEpisodePathSceneDto.setIndex(i);
                yrkScriptEpisodePathSceneDto.setContentStart(matcher.end(0)+1);
                yrkScriptEpisodePathSceneDto.setStart(matcher.start(0));
                list.add(yrkScriptEpisodePathSceneDto);
                //字符串截取
//                System.out.println("开始位置:" + matcher.start(0) + " 结束位置:" + matcher.end(0));
//                System.out.println(row);
                lastEnd = matcher.end(0);
                i++;
            }
        }
        if (i > 1) {
//                最后一段截取到结尾
            lastEnd = list.get(list.size() - 1).getContentStart();
//                content.substring(matcher.end(0), lastEnd);
            list.get(list.size() - 1).setContentEnd(episode.length());
            list.get(list.size() - 1).setContent(episode.substring(lastEnd ).trim());
        }
        return list;
    }

    /**
     * 剧本场次摘出属性 例如 角色：杀手，群众，
     *
     * @param attribute
     * @param episode
     * @paramdoEnd 是否匹配到结尾 否则只匹配到行
     * @return
     */
    @Override
    public String pathAttribute(String attribute, String episode,Boolean doEnd) {
        //替换中文字符
        String regex = attribute + "：.*[\\r|\\n]";
        if(doEnd){
            regex = regex+"*[\\W\\w]*";
        }
        Pattern p = Pattern.compile(regex);
        Matcher matcher = p.matcher(episode);
//        matcher.matches();
        String row = null;
        while (matcher.find()) {
            row = matcher.group();
//            if (row.length() > 3) {

//                if(!doEnd){
//                    匹配单行的时候去掉换行符
//                    row = StrUtils.delHuanhangAndKong(row);
//                }
//                删除属性字段，只留内容
//                row = row.replace(attribute + "：", "").trim();
//            }
        }
        return row;
    }
    @Override
    public String pathAttribute(String attribute, String episode) {
        return pathAttribute(attribute,episode,false);
    }
}
