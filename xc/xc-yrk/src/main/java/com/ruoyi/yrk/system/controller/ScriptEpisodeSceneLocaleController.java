package com.ruoyi.yrk.system.controller;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.yrk.system.domain.ScriptEpisodeSceneLocale;
import com.ruoyi.yrk.system.dto.ScriptEpisodeSceneLocaleDto;
import com.ruoyi.yrk.system.service.IScriptEpisodeSceneLocaleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;


/**
 * 剧本分场剧情关联地点Controller
 *
 * @author yichen
 * @date 2021-11-21
 */
@RestController
@RequestMapping("/yrk/scriptEpisodeSceneLocale")
public class ScriptEpisodeSceneLocaleController extends BaseController
{
    @Autowired
    private IScriptEpisodeSceneLocaleService yrkScriptEpisodeSceneLocaleService;

/**
 * 查询剧本分场剧情关联地点列表
 */
@PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneLocale:list')")
@GetMapping("/list")
        public TableDataInfo list(ScriptEpisodeSceneLocaleDto yrkScriptEpisodeSceneLocale)
    {
        startPage();
        List<ScriptEpisodeSceneLocale> list = yrkScriptEpisodeSceneLocaleService.selectdList(yrkScriptEpisodeSceneLocale);
        return getDataTable(list);
    }
    
    /**
     * 导出剧本分场剧情关联地点列表
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneLocale:export')")
    @Log(title = "剧本分场剧情关联地点", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ScriptEpisodeSceneLocaleDto yrkScriptEpisodeSceneLocale) throws IOException
    {

        startPage();
        List<ScriptEpisodeSceneLocale> list = yrkScriptEpisodeSceneLocaleService.selectdList(yrkScriptEpisodeSceneLocale);
        ExcelUtil<ScriptEpisodeSceneLocale> util = new ExcelUtil<ScriptEpisodeSceneLocale>(ScriptEpisodeSceneLocale.class);
        util.exportExcel(response, list, "剧本分场剧情关联地点数据");
    }
    /**
     * 导入剧本分场剧情关联地点
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneLocale:importData')")
    @Log(title = "剧本分场剧情关联地点导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<ScriptEpisodeSceneLocale> util = new ExcelUtil<ScriptEpisodeSceneLocale>( ScriptEpisodeSceneLocale.class);
        List<ScriptEpisodeSceneLocale> list = util.importExcel(file.getInputStream());
        boolean message=false;
        if(updateSupport){
            message =yrkScriptEpisodeSceneLocaleService.saveOrUpdateBatch(list);
        }else {
            message =yrkScriptEpisodeSceneLocaleService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }
    /**
     * 获取剧本分场剧情关联地点详细信息
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneLocale:query')")
    @GetMapping(value = "/{sceneId}")
    public AjaxResult getInfo(@PathVariable("sceneId") Long sceneId)
    {
        return AjaxResult.success(yrkScriptEpisodeSceneLocaleService.getById(sceneId));
    }

    /**
     * 新增剧本分场剧情关联地点
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneLocale:add')")
    @Log(title = "剧本分场剧情关联地点", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ScriptEpisodeSceneLocale yrkScriptEpisodeSceneLocale)
    {
        return toAjax(yrkScriptEpisodeSceneLocaleService.save(yrkScriptEpisodeSceneLocale));
    }

    /**
     * 修改剧本分场剧情关联地点
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneLocale:edit')")
    @Log(title = "剧本分场剧情关联地点", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ScriptEpisodeSceneLocale yrkScriptEpisodeSceneLocale)
    {
        return toAjax(yrkScriptEpisodeSceneLocaleService.updateById(yrkScriptEpisodeSceneLocale));
    }

    /**
     * 删除剧本分场剧情关联地点
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneLocale:remove')")
    @Log(title = "剧本分场剧情关联地点", businessType = BusinessType.DELETE)
    @DeleteMapping("/{sceneIds}")
    public AjaxResult remove(@PathVariable List<Long> sceneIds)
    {
        return toAjax(yrkScriptEpisodeSceneLocaleService.removeByIds(sceneIds));
    }
}
