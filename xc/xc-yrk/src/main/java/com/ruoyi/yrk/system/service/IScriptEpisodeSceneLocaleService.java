package com.ruoyi.yrk.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.yrk.system.domain.ScriptEpisodeSceneLocale;
import com.ruoyi.yrk.system.dto.ScriptEpisodeSceneLocaleDto;
/**
 * 剧本分场剧情关联地点Service接口
 * 
 * @author yichen
 * @date 2021-11-21
 */
public interface IScriptEpisodeSceneLocaleService extends IService<ScriptEpisodeSceneLocale>
{


    /**
     * 查询剧本分场剧情关联地点列表
     * 
     * @param yrkScriptEpisodeSceneLocale 剧本分场剧情关联地点
     * @return 剧本分场剧情关联地点集合
     */
    public List<ScriptEpisodeSceneLocale> selectdList(ScriptEpisodeSceneLocaleDto yrkScriptEpisodeSceneLocale);

}
