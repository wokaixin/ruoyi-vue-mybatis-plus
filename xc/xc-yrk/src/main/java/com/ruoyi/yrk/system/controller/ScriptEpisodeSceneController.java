package com.ruoyi.yrk.system.controller;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.yrk.system.domain.ScriptEpisodeScene;
import com.ruoyi.yrk.system.dto.ScriptEpisodeSceneDto;
import com.ruoyi.yrk.system.service.IScriptEpisodeSceneService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;


/**
 * 分场剧情Controller
 *
 * @author ruoyi
 * @date 2021-11-21
 */
@RestController
@RequestMapping("/yrk/scriptEpisodeScene")
public class ScriptEpisodeSceneController extends BaseController
{
    @Autowired
    private IScriptEpisodeSceneService yrkScriptEpisodeSceneService;

/**
 * 查询分场剧情列表
 */
@PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeScene:list')")
@GetMapping("/list")
        public TableDataInfo list(ScriptEpisodeSceneDto yrkScriptEpisodeScene)
    {
        startPage();
        List<ScriptEpisodeScene> list = yrkScriptEpisodeSceneService.selectdList(yrkScriptEpisodeScene);
        return getDataTable(list);
    }
    
    /**
     * 导出分场剧情列表
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeScene:export')")
    @Log(title = "分场剧情", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ScriptEpisodeSceneDto yrkScriptEpisodeScene) throws IOException
    {

        startPage();
        List<ScriptEpisodeScene> list = yrkScriptEpisodeSceneService.selectdList(yrkScriptEpisodeScene);
        ExcelUtil<ScriptEpisodeScene> util = new ExcelUtil<ScriptEpisodeScene>(ScriptEpisodeScene.class);
        util.exportExcel(response, list, "分场剧情数据");
    }

    /**
     * 导入分场剧情
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeScene:importData')")
    @Log(title = "分场剧情导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<ScriptEpisodeScene> util = new ExcelUtil<ScriptEpisodeScene>( ScriptEpisodeScene.class);
        List<ScriptEpisodeScene> list = util.importExcel(file.getInputStream());
        boolean message=false;
        if(updateSupport){
            message =yrkScriptEpisodeSceneService.saveOrUpdateBatch(list);
        }else {
            message =yrkScriptEpisodeSceneService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }
    /**
     * 获取分场剧情详细信息
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeScene:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(yrkScriptEpisodeSceneService.getById(id));
    }

    /**
     * 新增分场剧情
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeScene:add')")
    @Log(title = "分场剧情", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ScriptEpisodeScene yrkScriptEpisodeScene)
    {
        return toAjax(yrkScriptEpisodeSceneService.save(yrkScriptEpisodeScene));
    }

    /**
     * 修改分场剧情
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeScene:edit')")
    @Log(title = "分场剧情", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ScriptEpisodeScene yrkScriptEpisodeScene)
    {
        return toAjax(yrkScriptEpisodeSceneService.updateById(yrkScriptEpisodeScene));
    }

    /**
     * 删除分场剧情
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeScene:remove')")
    @Log(title = "分场剧情", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids)
    {
        return toAjax(yrkScriptEpisodeSceneService.removeByIds(ids));
    }
}
