package com.ruoyi.yrk.system.dto;

import lombok.Data;
import java.util.Date;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.dto.XDtoBaseEntity;

/**
 * 剧本分场剧情关联地点对象 yrk_script_episode_scene_locale
 * 
 * @author yichen
 * @date 2021-11-21
 */
@Data
public class ScriptEpisodeSceneLocaleDto extends  XDtoBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 分场id */
    private Long sceneId;
            /** 景点id */
    private Long localeId;
        


}
