package com.ruoyi.yrk.system.controller;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.yrk.system.domain.ScriptLocale;
import com.ruoyi.yrk.system.dto.ScriptLocaleDto;
import com.ruoyi.yrk.system.service.IScriptLocaleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;


/**
 * 剧本场景Controller
 *
 * @author yichen
 * @date 2021-11-21
 */
@RestController
@RequestMapping("/yrk/scriptLocale")
public class ScriptLocaleController extends BaseController
{
    @Autowired
    private IScriptLocaleService yrkScriptLocaleService;

/**
 * 查询剧本场景列表
 */
@PreAuthorize("@ss.hasPermi('yrk:scriptLocale:list')")
@GetMapping("/list")
        public TableDataInfo list(ScriptLocaleDto yrkScriptLocale)
    {
        startPage();
        List<ScriptLocale> list = yrkScriptLocaleService.selectdList(yrkScriptLocale);
        return getDataTable(list);
    }
    
    /**
     * 导出剧本场景列表
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptLocale:export')")
    @Log(title = "剧本场景", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ScriptLocaleDto yrkScriptLocale) throws IOException
    {

        startPage();
        List<ScriptLocale> list = yrkScriptLocaleService.selectdList(yrkScriptLocale);
        ExcelUtil<ScriptLocale> util = new ExcelUtil<ScriptLocale>(ScriptLocale.class);
        util.exportExcel(response, list, "剧本场景数据");
    }
    /**
     * 导入剧本场景
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptLocale:importData')")
    @Log(title = "剧本场景导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<ScriptLocale> util = new ExcelUtil<ScriptLocale>( ScriptLocale.class);
        List<ScriptLocale> list = util.importExcel(file.getInputStream());
        boolean message=false;
        if(updateSupport){
            message =yrkScriptLocaleService.saveOrUpdateBatch(list);
        }else {
            message =yrkScriptLocaleService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }
    /**
     * 获取剧本场景详细信息
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptLocale:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(yrkScriptLocaleService.getById(id));
    }

    /**
     * 新增剧本场景
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptLocale:add')")
    @Log(title = "剧本场景", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ScriptLocale yrkScriptLocale)
    {
        return toAjax(yrkScriptLocaleService.save(yrkScriptLocale));
    }

    /**
     * 修改剧本场景
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptLocale:edit')")
    @Log(title = "剧本场景", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ScriptLocale yrkScriptLocale)
    {
        return toAjax(yrkScriptLocaleService.updateById(yrkScriptLocale));
    }

    /**
     * 删除剧本场景
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptLocale:remove')")
    @Log(title = "剧本场景", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids)
    {
        return toAjax(yrkScriptLocaleService.removeByIds(ids));
    }
}
