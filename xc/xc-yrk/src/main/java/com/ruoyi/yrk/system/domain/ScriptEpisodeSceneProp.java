package com.ruoyi.yrk.system.domain;

import lombok.Data;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.domain.XBaseEntity;

/**
 * 剧本分场剧情关联道具对象 yrk_script_episode_scene_prop
 * 
 * @author yichen
 * @date 2021-11-21
 */
@Data
public class ScriptEpisodeSceneProp extends  XBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 分场id */
    @Excel(name = "分场id")
    private Long sceneId;
    /** 道具id */
    @Excel(name = "道具id")
    private Long propId;
}
