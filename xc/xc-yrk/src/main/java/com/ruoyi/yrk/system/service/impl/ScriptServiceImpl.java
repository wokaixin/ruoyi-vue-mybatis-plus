package com.ruoyi.yrk.system.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.yrk.system.mapper.ScriptMapper;
import com.ruoyi.yrk.system.domain.Script;
import com.ruoyi.yrk.system.dto.ScriptDto;
import com.ruoyi.yrk.system.service.IScriptService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 * 剧本基础信息Service业务层处理
 *
 * @author yichen
 * @date 2021-11-21
 */
@Service
public class ScriptServiceImpl extends ServiceImpl<ScriptMapper, Script> implements IScriptService {
    @Autowired
    private ScriptMapper yrkScriptMapper;

    /**
     * 查询剧本基础信息列表
     * 
     * @param yrkScript 剧本基础信息
     * @return 剧本基础信息
     */
    @Override
    public List<Script> selectdList(ScriptDto yrkScript)
    {
        QueryWrapper<Script> q= new QueryWrapper<>();
        if(yrkScript.getId()!=null  ){
            q.eq("id",yrkScript.getId());
        }
        if(yrkScript.getName()!=null   && !yrkScript.getName().trim().equals("")){
            q.like("name",yrkScript.getName());
        }
        if(yrkScript.getUserId()!=null  ){
            q.eq("user_id",yrkScript.getUserId());
        }
        if(yrkScript.getAuthor()!=null   && !yrkScript.getAuthor().trim().equals("")){
            q.like("author",yrkScript.getAuthor());
        }
        if(yrkScript.getBeginCreateTime()!=null && yrkScript.getEndCreateTime()!=null){
            q.between("create_time",yrkScript.getBeginCreateTime(),yrkScript.getEndCreateTime());
        }
        if(yrkScript.getBeginUpdateTime()!=null && yrkScript.getEndUpdateTime()!=null){
            q.between("update_time",yrkScript.getBeginUpdateTime(),yrkScript.getEndUpdateTime());
        }
        if(yrkScript.getBeginDeleteTime()!=null && yrkScript.getEndDeleteTime()!=null){
            q.between("delete_time",yrkScript.getBeginDeleteTime(),yrkScript.getEndDeleteTime());
        }
        if(yrkScript.getStatus()!=null  ){
            q.eq("status",yrkScript.getStatus());
        }
        List<Script> list = this.list(q);
        return list;
    }


}
