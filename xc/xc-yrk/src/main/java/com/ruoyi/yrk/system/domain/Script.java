package com.ruoyi.yrk.system.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.domain.XBaseEntity;

/**
 * 剧本基础信息对象 yrk_script
 * 
 * @author yichen
 * @date 2021-11-21
 */
@Data
public class Script extends  XBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** id */
    @TableId(type= IdType.AUTO)
    private Long id;
    /** 剧本名称 */
    @Excel(name = "剧本名称")
    private String name;
    /** 描述 */
    @Excel(name = "描述")
    private String describes;
    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;
    /** 作者 */
    @Excel(name = "作者")
    private String author;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deleteTime;
    /** 状态 */
    @Excel(name = "状态")
    private Integer status;
    /** 备注 */
    @Excel(name = "备注")
    private String notes;
    /** 更多图片 */
    @Excel(name = "更多图片")
    private String images;
    /** 海报 */
    @Excel(name = "海报")
    private String poster;
}
