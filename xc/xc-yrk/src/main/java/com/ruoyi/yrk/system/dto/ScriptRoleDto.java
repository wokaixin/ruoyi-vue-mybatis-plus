package com.ruoyi.yrk.system.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.domain.XBaseEntity;
import lombok.Data;

/**
 * 剧本角色对象 yrk_script_role
 * 
 * @author yichen
 * @date 2021-11-21
 */
@Data
public class ScriptRoleDto extends XBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** id */
    private Long id;
            /** 名称 */
    private String name;
    /** 描述 */
    @Excel(name = "描述")
    private String describes;
            /** 年龄 */
    private Long age;
            /** 性别 */
    private Integer sex;
            /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginCreateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endCreateTime;
        /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginUpdateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endUpdateTime;
        /** 状态 */
    private Integer status;
            /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginDeleteTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDeleteTime;
    


}
