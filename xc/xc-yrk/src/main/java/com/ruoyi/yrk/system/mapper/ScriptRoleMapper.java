package com.ruoyi.yrk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.yrk.system.domain.ScriptRole;

/**
 * 剧本角色Mapper接口
 * 
 * @author yichen
 * @date 2021-11-21
 */
public interface ScriptRoleMapper extends BaseMapper<ScriptRole>
{



}
