package com.ruoyi.yrk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.yrk.system.domain.ScriptEpisodeSceneRole;

/**
 * 剧本分场剧情关联角色Mapper接口
 * 
 * @author yichen
 * @date 2021-11-21
 */
public interface ScriptEpisodeSceneRoleMapper extends BaseMapper<ScriptEpisodeSceneRole>
{



}
