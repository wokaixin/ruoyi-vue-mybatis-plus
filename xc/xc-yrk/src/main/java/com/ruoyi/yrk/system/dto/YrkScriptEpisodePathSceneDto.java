package com.ruoyi.yrk.system.dto;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.xingchen.common.dto.XDtoBaseEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 分集剧情对象 yrk_script_episode
 * 
 * @author yichen
 * @date 2021-11-21
 */
@Data
public class YrkScriptEpisodePathSceneDto extends  XDtoBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 所在本集排序号*/
    private Integer index;
    /** 场次所在本集位置开始处*/
    private Integer start;
    /** 内容所在本集位置开始处*/
    private Integer contentStart;
    /** 内容所在本集位置结束处*/
    private Integer contentEnd;
    /** 场次标题 */
    private String title;
    private Integer contentSize;
//    /** 时间 */
//    private String time;
//    /** 角色 */
//    private String[] role;
//    /** 场景 */
//    private String[] locale;
//    /** 道具 */
//    private String[] prop;
    private Map<String, List<String>> attributes;
//    /** 气象 */
//    private String weather;
//    /** 景别:内外景 */
//    private String Landscape;
    /** 内容 */
    private String content;


}
