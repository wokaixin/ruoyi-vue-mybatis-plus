package com.ruoyi.yrk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.yrk.system.domain.ScriptProp;

/**
 * 剧本道具Mapper接口
 * 
 * @author yichen
 * @date 2021-11-21
 */
public interface ScriptPropMapper extends BaseMapper<ScriptProp>
{



}
