package com.ruoyi.yrk.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.yrk.system.domain.ScriptEpisodeSceneProp;
import com.ruoyi.yrk.system.dto.ScriptEpisodeScenePropDto;
/**
 * 剧本分场剧情关联道具Service接口
 * 
 * @author yichen
 * @date 2021-11-21
 */
public interface IScriptEpisodeScenePropService extends IService<ScriptEpisodeSceneProp>
{


    /**
     * 查询剧本分场剧情关联道具列表
     * 
     * @param yrkScriptEpisodeSceneProp 剧本分场剧情关联道具
     * @return 剧本分场剧情关联道具集合
     */
    public List<ScriptEpisodeSceneProp> selectdList(ScriptEpisodeScenePropDto yrkScriptEpisodeSceneProp);

}
