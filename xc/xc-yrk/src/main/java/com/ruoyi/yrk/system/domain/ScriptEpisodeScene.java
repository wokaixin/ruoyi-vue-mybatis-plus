package com.ruoyi.yrk.system.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.domain.XBaseEntity;

/**
 * 分场剧情对象 yrk_script_episode_scene
 * 
 * @author ruoyi
 * @date 2021-11-21
 */
@Data
public class ScriptEpisodeScene extends  XBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** id */
    @Excel(name = "id")
    @TableId(type= IdType.AUTO)
    private Long id;
    /** 标题 */
    @Excel(name = "标题")
    private String title;
    /** 描述 */
    @Excel(name = "描述")
    private String describes;
    /** 内容 */
    @Excel(name = "内容")
    private String content;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    /** 集id */
    @Excel(name = "集id")
    private Long episodeId;
    /** 场次 */
    @Excel(name = "场次")
    private String number;
    /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deleteTime;
    /** 备注 */
    @Excel(name = "备注")
    private String notes;
}
