package com.ruoyi.yrk.system.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.dto.XDtoBaseEntity;

/**
 * 分集剧情对象 yrk_script_episode
 * 
 * @author yichen
 * @date 2021-11-21
 */
@Data
public class ScriptEpisodeDto extends  XDtoBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** id */
    private Long id;
            /** 集数 */
    private Long number;
    /** 描述 */
    @Excel(name = "描述")
    private String describes;
            /** 标题 */
    private String title;
            /** 剧本id */
    private Long scriptId;
            /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginCreateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endCreateTime;
        /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginUpdateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endUpdateTime;
        /** 状态 */
    private Long status;
            /** 内容 */
    private String content;
            /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginDeleteTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDeleteTime;
    


}
