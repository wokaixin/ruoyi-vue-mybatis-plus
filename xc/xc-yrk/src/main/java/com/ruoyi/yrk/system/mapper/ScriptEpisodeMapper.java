package com.ruoyi.yrk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.yrk.system.domain.ScriptEpisode;

/**
 * 分集剧情Mapper接口
 * 
 * @author yichen
 * @date 2021-11-21
 */
public interface ScriptEpisodeMapper extends BaseMapper<ScriptEpisode>
{



}
