package com.ruoyi.yrk.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.yrk.system.domain.ScriptRole;
import com.ruoyi.yrk.system.dto.ScriptRoleDto;
/**
 * 剧本角色Service接口
 * 
 * @author yichen
 * @date 2021-11-21
 */
public interface IScriptRoleService extends IService<ScriptRole>
{


    /**
     * 查询剧本角色列表
     * 
     * @param yrkScriptRole 剧本角色
     * @return 剧本角色集合
     */
    public List<ScriptRole> selectdList(ScriptRoleDto yrkScriptRole);

}
