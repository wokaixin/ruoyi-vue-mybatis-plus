package com.ruoyi.yrk.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.domain.XBaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 剧本角色对象 yrk_script_role
 * 
 * @author yichen
 * @date 2021-11-21
 */
@Data
public class ScriptRole extends XBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** id */
    @TableId(type= IdType.AUTO)
    private Long id;
    /** 名称 */
    @Excel(name = "名称")
    private String name;
    /** 年龄 */
    @Excel(name = "年龄")
    private Long age;
    /** 性别 */
    @Excel(name = "性别")
    private Integer sex;
    /** 描述 */
    @Excel(name = "描述")
    private String describes;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    /** 状态 */
    @Excel(name = "状态")
    private Integer status;
    /** 角色类型 */
    @Excel(name = "角色类型")
    private String type;
    /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deleteTime;
    /** 备注 */
    @Excel(name = "备注")
    private String notes;
    /** 父id */
    @Excel(name = "父id")
    private Long pid;
    /** 更多图片 */
    @Excel(name = "更多图片")
    private String images;
    /** 海报 */
    @Excel(name = "海报")
    private String poster;
}
