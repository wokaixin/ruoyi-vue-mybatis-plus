package com.ruoyi.yrk.system.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
@Data
public class YrkScriptEpisodeImportDocDto {
    MultipartFile file;
//    属性列表
    List<String> attributes;
//    是否更新
    Boolean updateSupport;
//    scriptEpisodeId;
//    剧本id
    Integer scriptId;
}
