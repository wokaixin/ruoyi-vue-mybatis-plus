package com.ruoyi.yrk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.yrk.system.domain.ScriptEpisodeSceneProp;

/**
 * 剧本分场剧情关联道具Mapper接口
 * 
 * @author yichen
 * @date 2021-11-21
 */
public interface ScriptEpisodeScenePropMapper extends BaseMapper<ScriptEpisodeSceneProp>
{



}
