package com.ruoyi.yrk.system.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.yrk.system.mapper.ScriptLocaleMapper;
import com.ruoyi.yrk.system.domain.ScriptLocale;
import com.ruoyi.yrk.system.dto.ScriptLocaleDto;
import com.ruoyi.yrk.system.service.IScriptLocaleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 * 剧本场景Service业务层处理
 *
 * @author yichen
 * @date 2021-11-21
 */
@Service
public class ScriptLocaleServiceImpl extends ServiceImpl<ScriptLocaleMapper, ScriptLocale> implements IScriptLocaleService {
    @Autowired
    private ScriptLocaleMapper yrkScriptLocaleMapper;

    /**
     * 查询剧本场景列表
     * 
     * @param yrkScriptLocale 剧本场景
     * @return 剧本场景
     */
    @Override
    public List<ScriptLocale> selectdList(ScriptLocaleDto yrkScriptLocale)
    {
        QueryWrapper<ScriptLocale> q= new QueryWrapper<>();
        if(yrkScriptLocale.getId()!=null  ){
            q.eq("id",yrkScriptLocale.getId());
        }
        if(yrkScriptLocale.getScriptId()!=null  ){
            q.eq("script_id",yrkScriptLocale.getScriptId());
        }
        if(yrkScriptLocale.getName()!=null   && !yrkScriptLocale.getName().trim().equals("")){
            q.like("name",yrkScriptLocale.getName());
        }
        if(yrkScriptLocale.getBeginCreateTime()!=null && yrkScriptLocale.getEndCreateTime()!=null){
            q.between("create_time",yrkScriptLocale.getBeginCreateTime(),yrkScriptLocale.getEndCreateTime());
        }
        if(yrkScriptLocale.getBeginUpdateTime()!=null && yrkScriptLocale.getEndUpdateTime()!=null){
            q.between("update_time",yrkScriptLocale.getBeginUpdateTime(),yrkScriptLocale.getEndUpdateTime());
        }
        if(yrkScriptLocale.getBeginDeleteTime()!=null && yrkScriptLocale.getEndDeleteTime()!=null){
            q.between("delete_time",yrkScriptLocale.getBeginDeleteTime(),yrkScriptLocale.getEndDeleteTime());
        }
        if(yrkScriptLocale.getStatus()!=null  ){
            q.eq("status",yrkScriptLocale.getStatus());
        }
        if(yrkScriptLocale.getPid()!=null  ){
            q.eq("pid",yrkScriptLocale.getPid());
        }
        List<ScriptLocale> list = this.list(q);
        return list;
    }


}
