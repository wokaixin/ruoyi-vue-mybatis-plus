package com.ruoyi.yrk.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.yrk.system.domain.Script;

/**
 * 剧本基础信息Mapper接口
 * 
 * @author yichen
 * @date 2021-11-21
 */
public interface ScriptMapper extends BaseMapper<Script>
{



}
