package com.ruoyi.yrk.system.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.dto.XDtoBaseEntity;

/**
 * 分场剧情对象 yrk_script_episode_scene
 * 
 * @author ruoyi
 * @date 2021-11-21
 */
@Data
public class ScriptEpisodeSceneDto extends  XDtoBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** id */
    private Long id;
            /** 标题 */
    private String title;
    /** 描述 */
    @Excel(name = "描述")
    private String describes;
            /** 内容 */
    private String content;
            /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginCreateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endCreateTime;
        /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginUpdateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endUpdateTime;
        /** 集id */
    private Long episodeId;
            /** 场次 */
    private String number;
            /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginDeleteTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDeleteTime;
    


}
