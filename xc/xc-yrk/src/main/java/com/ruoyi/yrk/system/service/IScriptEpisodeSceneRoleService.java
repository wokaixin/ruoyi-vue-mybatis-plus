package com.ruoyi.yrk.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.yrk.system.domain.ScriptEpisodeSceneRole;
import com.ruoyi.yrk.system.dto.ScriptEpisodeSceneRoleDto;
/**
 * 剧本分场剧情关联角色Service接口
 * 
 * @author yichen
 * @date 2021-11-21
 */
public interface IScriptEpisodeSceneRoleService extends IService<ScriptEpisodeSceneRole>
{


    /**
     * 查询剧本分场剧情关联角色列表
     * 
     * @param yrkScriptEpisodeSceneRole 剧本分场剧情关联角色
     * @return 剧本分场剧情关联角色集合
     */
    public List<ScriptEpisodeSceneRole> selectdList(ScriptEpisodeSceneRoleDto yrkScriptEpisodeSceneRole);

}
