package com.ruoyi.yrk.system.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.yrk.system.mapper.ScriptEpisodeSceneLocaleMapper;
import com.ruoyi.yrk.system.domain.ScriptEpisodeSceneLocale;
import com.ruoyi.yrk.system.dto.ScriptEpisodeSceneLocaleDto;
import com.ruoyi.yrk.system.service.IScriptEpisodeSceneLocaleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 * 剧本分场剧情关联地点Service业务层处理
 *
 * @author yichen
 * @date 2021-11-21
 */
@Service
<<<<<<< HEAD:xc/xc-yrk/src/main/java/com/ruoyi/yrk/system/service/impl/ScriptEpisodeSceneLocaleServiceImpl.java
public class ScriptEpisodeSceneLocaleServiceImpl extends ServiceImpl<ScriptEpisodeSceneLocaleMapper, ScriptEpisodeSceneLocale> implements IScriptEpisodeSceneLocaleService {
    @Autowired
    private ScriptEpisodeSceneLocaleMapper yrkScriptEpisodeSceneLocaleMapper;
=======
public class YrkScriptEpisodeSceneLocaleServiceImpl extends ServiceImpl<YrkScriptEpisodeSceneLocaleMapper, YrkScriptEpisodeSceneLocale> implements IYrkScriptEpisodeSceneLocaleService {
>>>>>>> 49dc978e45d24fe85f98d4de0c1313c812f25ad4:xingchen/xingchen-yrk/src/main/java/com/ruoyi/yrk/system/service/impl/YrkScriptEpisodeSceneLocaleServiceImpl.java

    /**
     * 查询剧本分场剧情关联地点列表
     * 
     * @param yrkScriptEpisodeSceneLocale 剧本分场剧情关联地点
     * @return 剧本分场剧情关联地点
     */
    @Override
    public List<ScriptEpisodeSceneLocale> selectdList(ScriptEpisodeSceneLocaleDto yrkScriptEpisodeSceneLocale)
    {
        QueryWrapper<ScriptEpisodeSceneLocale> q= new QueryWrapper<>();
        if(yrkScriptEpisodeSceneLocale.getSceneId()!=null  ){
            q.eq("scene_id",yrkScriptEpisodeSceneLocale.getSceneId());
        }
        if(yrkScriptEpisodeSceneLocale.getLocaleId()!=null  ){
            q.eq("locale_id",yrkScriptEpisodeSceneLocale.getLocaleId());
        }
        List<ScriptEpisodeSceneLocale> list = this.list(q);
        return list;
    }


}
