package com.ruoyi.yrk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.yrk.system.domain.ScriptLocale;

/**
 * 剧本场景Mapper接口
 * 
 * @author yichen
 * @date 2021-11-21
 */
public interface ScriptLocaleMapper extends BaseMapper<ScriptLocale>
{



}
