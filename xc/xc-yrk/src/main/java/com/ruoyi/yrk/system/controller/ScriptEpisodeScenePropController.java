package com.ruoyi.yrk.system.controller;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.yrk.system.domain.ScriptEpisodeSceneProp;
import com.ruoyi.yrk.system.dto.ScriptEpisodeScenePropDto;
import com.ruoyi.yrk.system.service.IScriptEpisodeScenePropService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;


/**
 * 剧本分场剧情关联道具Controller
 *
 * @author yichen
 * @date 2021-11-21
 */
@RestController
@RequestMapping("/yrk/scriptEpisodeSceneProp")
public class ScriptEpisodeScenePropController extends BaseController
{
    @Autowired
    private IScriptEpisodeScenePropService yrkScriptEpisodeScenePropService;

/**
 * 查询剧本分场剧情关联道具列表
 */
@PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneProp:list')")
@GetMapping("/list")
        public TableDataInfo list(ScriptEpisodeScenePropDto yrkScriptEpisodeSceneProp)
    {
        startPage();
        List<ScriptEpisodeSceneProp> list = yrkScriptEpisodeScenePropService.selectdList(yrkScriptEpisodeSceneProp);
        return getDataTable(list);
    }
    
    /**
     * 导出剧本分场剧情关联道具列表
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneProp:export')")
    @Log(title = "剧本分场剧情关联道具", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ScriptEpisodeScenePropDto yrkScriptEpisodeSceneProp) throws IOException
    {

        startPage();
        List<ScriptEpisodeSceneProp> list = yrkScriptEpisodeScenePropService.selectdList(yrkScriptEpisodeSceneProp);
        ExcelUtil<ScriptEpisodeSceneProp> util = new ExcelUtil<ScriptEpisodeSceneProp>(ScriptEpisodeSceneProp.class);
        util.exportExcel(response, list, "剧本分场剧情关联道具数据");
    }
    /**
     * 导入剧本分场剧情关联道具
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneProp:importData')")
    @Log(title = "剧本分场剧情关联道具导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<ScriptEpisodeSceneProp> util = new ExcelUtil<ScriptEpisodeSceneProp>( ScriptEpisodeSceneProp.class);
        List<ScriptEpisodeSceneProp> list = util.importExcel(file.getInputStream());
        boolean message=false;
        if(updateSupport){
            message =yrkScriptEpisodeScenePropService.saveOrUpdateBatch(list);
        }else {
            message =yrkScriptEpisodeScenePropService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }
    /**
     * 获取剧本分场剧情关联道具详细信息
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneProp:query')")
    @GetMapping(value = "/{sceneId}")
    public AjaxResult getInfo(@PathVariable("sceneId") Long sceneId)
    {
        return AjaxResult.success(yrkScriptEpisodeScenePropService.getById(sceneId));
    }

    /**
     * 新增剧本分场剧情关联道具
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneProp:add')")
    @Log(title = "剧本分场剧情关联道具", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ScriptEpisodeSceneProp yrkScriptEpisodeSceneProp)
    {
        return toAjax(yrkScriptEpisodeScenePropService.save(yrkScriptEpisodeSceneProp));
    }

    /**
     * 修改剧本分场剧情关联道具
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneProp:edit')")
    @Log(title = "剧本分场剧情关联道具", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ScriptEpisodeSceneProp yrkScriptEpisodeSceneProp)
    {
        return toAjax(yrkScriptEpisodeScenePropService.updateById(yrkScriptEpisodeSceneProp));
    }

    /**
     * 删除剧本分场剧情关联道具
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneProp:remove')")
    @Log(title = "剧本分场剧情关联道具", businessType = BusinessType.DELETE)
    @DeleteMapping("/{sceneIds}")
    public AjaxResult remove(@PathVariable List<Long> sceneIds)
    {
        return toAjax(yrkScriptEpisodeScenePropService.removeByIds(sceneIds));
    }
}
