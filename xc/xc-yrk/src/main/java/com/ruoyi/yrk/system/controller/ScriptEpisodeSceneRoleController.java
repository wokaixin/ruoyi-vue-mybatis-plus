package com.ruoyi.yrk.system.controller;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.yrk.system.domain.ScriptEpisodeSceneRole;
import com.ruoyi.yrk.system.dto.ScriptEpisodeSceneRoleDto;
import com.ruoyi.yrk.system.service.IScriptEpisodeSceneRoleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;


/**
 * 剧本分场剧情关联角色Controller
 *
 * @author yichen
 * @date 2021-11-21
 */
@RestController
@RequestMapping("/yrk/scriptEpisodeSceneRole")
public class ScriptEpisodeSceneRoleController extends BaseController
{
    @Autowired
    private IScriptEpisodeSceneRoleService yrkScriptEpisodeSceneRoleService;

/**
 * 查询剧本分场剧情关联角色列表
 */
@PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneRole:list')")
@GetMapping("/list")
        public TableDataInfo list(ScriptEpisodeSceneRoleDto yrkScriptEpisodeSceneRole)
    {
        startPage();
        List<ScriptEpisodeSceneRole> list = yrkScriptEpisodeSceneRoleService.selectdList(yrkScriptEpisodeSceneRole);
        return getDataTable(list);
    }
    
    /**
     * 导出剧本分场剧情关联角色列表
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneRole:export')")
    @Log(title = "剧本分场剧情关联角色", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ScriptEpisodeSceneRoleDto yrkScriptEpisodeSceneRole) throws IOException
    {

        startPage();
        List<ScriptEpisodeSceneRole> list = yrkScriptEpisodeSceneRoleService.selectdList(yrkScriptEpisodeSceneRole);
        ExcelUtil<ScriptEpisodeSceneRole> util = new ExcelUtil<ScriptEpisodeSceneRole>(ScriptEpisodeSceneRole.class);
        util.exportExcel(response, list, "剧本分场剧情关联角色数据");
    }
    /**
     * 导入剧本分场剧情关联角色
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneRole:importData')")
    @Log(title = "剧本分场剧情关联角色导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<ScriptEpisodeSceneRole> util = new ExcelUtil<ScriptEpisodeSceneRole>( ScriptEpisodeSceneRole.class);
        List<ScriptEpisodeSceneRole> list = util.importExcel(file.getInputStream());
        boolean message=false;
        if(updateSupport){
            message =yrkScriptEpisodeSceneRoleService.saveOrUpdateBatch(list);
        }else {
            message =yrkScriptEpisodeSceneRoleService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }
    /**
     * 获取剧本分场剧情关联角色详细信息
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneRole:query')")
    @GetMapping(value = "/{sceneId}")
    public AjaxResult getInfo(@PathVariable("sceneId") Long sceneId)
    {
        return AjaxResult.success(yrkScriptEpisodeSceneRoleService.getById(sceneId));
    }

    /**
     * 新增剧本分场剧情关联角色
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneRole:add')")
    @Log(title = "剧本分场剧情关联角色", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ScriptEpisodeSceneRole yrkScriptEpisodeSceneRole)
    {
        return toAjax(yrkScriptEpisodeSceneRoleService.save(yrkScriptEpisodeSceneRole));
    }

    /**
     * 修改剧本分场剧情关联角色
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneRole:edit')")
    @Log(title = "剧本分场剧情关联角色", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ScriptEpisodeSceneRole yrkScriptEpisodeSceneRole)
    {
        return toAjax(yrkScriptEpisodeSceneRoleService.updateById(yrkScriptEpisodeSceneRole));
    }

    /**
     * 删除剧本分场剧情关联角色
     */
    @PreAuthorize("@ss.hasPermi('yrk:scriptEpisodeSceneRole:remove')")
    @Log(title = "剧本分场剧情关联角色", businessType = BusinessType.DELETE)
    @DeleteMapping("/{sceneIds}")
    public AjaxResult remove(@PathVariable List<Long> sceneIds)
    {
        return toAjax(yrkScriptEpisodeSceneRoleService.removeByIds(sceneIds));
    }
}
