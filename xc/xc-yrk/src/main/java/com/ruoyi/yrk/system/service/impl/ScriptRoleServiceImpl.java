package com.ruoyi.yrk.system.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.yrk.system.mapper.ScriptRoleMapper;
import com.ruoyi.yrk.system.domain.ScriptRole;
import com.ruoyi.yrk.system.dto.ScriptRoleDto;
import com.ruoyi.yrk.system.service.IScriptRoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 * 剧本角色Service业务层处理
 *
 * @author yichen
 * @date 2021-11-21
 */
@Service
public class ScriptRoleServiceImpl extends ServiceImpl<ScriptRoleMapper, ScriptRole> implements IScriptRoleService {
    @Autowired
    private ScriptRoleMapper yrkScriptRoleMapper;

    /**
     * 查询剧本角色列表
     * 
     * @param yrkScriptRole 剧本角色
     * @return 剧本角色
     */
    @Override
    public List<ScriptRole> selectdList(ScriptRoleDto yrkScriptRole)
    {
        QueryWrapper<ScriptRole> q= new QueryWrapper<>();
        if(yrkScriptRole.getId()!=null  ){
            q.eq("id",yrkScriptRole.getId());
        }
        if(yrkScriptRole.getName()!=null   && !yrkScriptRole.getName().trim().equals("")){
            q.like("name",yrkScriptRole.getName());
        }
        if(yrkScriptRole.getAge()!=null  ){
            q.eq("age",yrkScriptRole.getAge());
        }
        if(yrkScriptRole.getSex()!=null  ){
            q.eq("sex",yrkScriptRole.getSex());
        }
        if(yrkScriptRole.getBeginCreateTime()!=null && yrkScriptRole.getEndCreateTime()!=null){
            q.between("create_time",yrkScriptRole.getBeginCreateTime(),yrkScriptRole.getEndCreateTime());
        }
        if(yrkScriptRole.getBeginUpdateTime()!=null && yrkScriptRole.getEndUpdateTime()!=null){
            q.between("update_time",yrkScriptRole.getBeginUpdateTime(),yrkScriptRole.getEndUpdateTime());
        }
        if(yrkScriptRole.getStatus()!=null  ){
            q.eq("status",yrkScriptRole.getStatus());
        }
        if(yrkScriptRole.getBeginDeleteTime()!=null && yrkScriptRole.getEndDeleteTime()!=null){
            q.between("delete_time",yrkScriptRole.getBeginDeleteTime(),yrkScriptRole.getEndDeleteTime());
        }
        List<ScriptRole> list = this.list(q);
        return list;
    }


}
