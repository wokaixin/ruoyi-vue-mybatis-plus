package com.ruoyi.yrk.system.service.impl;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.yrk.system.mapper.ScriptEpisodeMapper;
import com.ruoyi.yrk.system.domain.ScriptEpisode;
import com.ruoyi.yrk.system.dto.ScriptEpisodeDto;
import com.ruoyi.yrk.system.service.IScriptEpisodeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 * 分集剧情Service业务层处理
 *
 * @author yichen
 * @date 2021-11-21
 */
@Service
<<<<<<< HEAD:xc/xc-yrk/src/main/java/com/ruoyi/yrk/system/service/impl/ScriptEpisodeServiceImpl.java
public class ScriptEpisodeServiceImpl extends ServiceImpl<ScriptEpisodeMapper, ScriptEpisode> implements IScriptEpisodeService {
    @Autowired
    private ScriptEpisodeMapper yrkScriptEpisodeMapper;
=======
public class YrkScriptEpisodeServiceImpl extends ServiceImpl<YrkScriptEpisodeMapper, YrkScriptEpisode> implements IYrkScriptEpisodeService {

>>>>>>> 49dc978e45d24fe85f98d4de0c1313c812f25ad4:xingchen/xingchen-yrk/src/main/java/com/ruoyi/yrk/system/service/impl/YrkScriptEpisodeServiceImpl.java

    /**
     * 查询分集剧情列表
     * 
     * @param yrkScriptEpisode 分集剧情
     * @return 分集剧情
     */
    @Override
    public List<ScriptEpisode> selectdList(ScriptEpisodeDto yrkScriptEpisode)
    {
        QueryWrapper<ScriptEpisode> q= new QueryWrapper<>();
        if(yrkScriptEpisode.getId()!=null  ){
            q.eq("id",yrkScriptEpisode.getId());
        }
        if(yrkScriptEpisode.getNumber()!=null  ){
            q.eq("number",yrkScriptEpisode.getNumber());
        }
        if(yrkScriptEpisode.getTitle()!=null   && !yrkScriptEpisode.getTitle().trim().equals("")){
            q.like("title",yrkScriptEpisode.getTitle());
        }
        if(yrkScriptEpisode.getScriptId()!=null  ){
            q.eq("script_id",yrkScriptEpisode.getScriptId());
        }
        if(yrkScriptEpisode.getBeginCreateTime()!=null && yrkScriptEpisode.getEndCreateTime()!=null){
            q.between("create_time",yrkScriptEpisode.getBeginCreateTime(),yrkScriptEpisode.getEndCreateTime());
        }
        if(yrkScriptEpisode.getBeginUpdateTime()!=null && yrkScriptEpisode.getEndUpdateTime()!=null){
            q.between("update_time",yrkScriptEpisode.getBeginUpdateTime(),yrkScriptEpisode.getEndUpdateTime());
        }
        if(yrkScriptEpisode.getStatus()!=null  ){
            q.eq("status",yrkScriptEpisode.getStatus());
        }
        if(yrkScriptEpisode.getContent()!=null   && !yrkScriptEpisode.getContent().trim().equals("")){
            q.like("content",yrkScriptEpisode.getContent());
        }
        if(yrkScriptEpisode.getBeginDeleteTime()!=null && yrkScriptEpisode.getEndDeleteTime()!=null){
            q.between("delete_time",yrkScriptEpisode.getBeginDeleteTime(),yrkScriptEpisode.getEndDeleteTime());
        }
        List<ScriptEpisode> list = this.list(q);
        return list;
    }


}
