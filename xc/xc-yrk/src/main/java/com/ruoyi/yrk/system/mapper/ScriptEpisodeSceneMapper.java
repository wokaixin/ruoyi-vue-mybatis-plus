package com.ruoyi.yrk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.yrk.system.domain.ScriptEpisodeScene;

/**
 * 分场剧情Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-21
 */
public interface ScriptEpisodeSceneMapper extends BaseMapper<ScriptEpisodeScene>
{



}
