package com.ruoyi.qz.system.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.vo.XVoBaseEntity;

/**
 * 用户对象 lngx_user
 * 
 * @author yichen
 * @date 2021-10-17
 */
@Data
public class UserVo extends  XVoBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 用户id */
    @Excel(name = "用户id")
    private Long uid;
    /** 用户名 */
    @Excel(name = "用户名")
    private String username;
    /** 密码 */
    @Excel(name = "密码")
    private String password;
    /** 昵称 */
    @Excel(name = "昵称")
    private String nickname;
    /** 用户组id */
    @Excel(name = "用户组id")
    private Long groupId;
    /** 头像 */
    @Excel(name = "头像")
    private String avatar;
    /** 性别 */
    @Excel(name = "性别")
    private Integer gender;
    /** 省 */
    @Excel(name = "省")
    private String province;
    /** 城市 */
    @Excel(name = "城市")
    private String city;
    /** 开发平台id */
    @Excel(name = "开发平台id")
    private String openid;
    /** 状态 */
    @Excel(name = "状态")
    private Integer status;
    /** 介绍 */
    @Excel(name = "介绍")
    private String intro;
    /** 积分 */
    @Excel(name = "积分")
    private Long integral;
    /** websocket用户Id多个逗号分割 */
    @Excel(name = "websocket用户Id多个逗号分割")
    private Long fd;
    /** 上次登陆IP */
    @Excel(name = "上次登陆IP")
    private String lastLoginIp;
    /** 更新时间 */
    private Date updateTime;
    /** 用户标签 */
    @Excel(name = "用户标签")
    private String tagStr;
    /** 0为普通用户，1为官方账号 */
    @Excel(name = "0为普通用户，1为官方账号")
    private Integer type;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deleteTime;
    /** 用户wss链接ip:fd 多个逗号分割 */
    @Excel(name = "用户wss链接ip:fd 多个逗号分割")
    private String wss;
    /** 加密值 */
    @Excel(name = "加密值")
    private String secret;
    /** 开放平台id */
    @Excel(name = "开放平台id")
    private String unionid;
}
