package com.ruoyi.qz.system.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.xc.common.dto.XDtoBaseEntity;

/**
 * 话题对象 lngx_topic
 * 
 * @author yichen
 * @date 2021-10-17
 */
@Data
public class TopicDto extends  XDtoBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 主键id */
    private Long id;
            /** 用户id */
    private Long uid;
            /** 分类id */
    private Long cateId;
            /** 话题名称 */
    private String topicName;
            /** 推荐类型：0 不推荐 1首页推荐 */
    private Integer topType;
            /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginCreateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endCreateTime;
        /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginDeleteTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDeleteTime;
        /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginUpdateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endUpdateTime;
    


}
