package com.ruoyi.qz.system.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.qz.system.mapper.TopicCateMapper;
import com.ruoyi.qz.system.domain.TopicCate;
import com.ruoyi.qz.system.dto.TopicCateDto;
import com.ruoyi.qz.system.service.ITopicCateService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 * 话题分类Service业务层处理
 *
 * @author yichen
 * @date 2021-10-17
 */
@Service
public class LngxTopicCateServiceImpl extends ServiceImpl<TopicCateMapper, TopicCate> implements ITopicCateService {
    @Autowired
    private TopicCateMapper lngxTopicCateMapper;

    /**
     * 查询话题分类列表
     * 
     * @param lngxTopicCate 话题分类
     * @return 话题分类
     */
    @Override
    public List<TopicCate> selectdList(TopicCateDto lngxTopicCate)
    {
        QueryWrapper<TopicCate> q= new QueryWrapper<>();
        if(lngxTopicCate.getId()!=null  ){
            q.eq("id",lngxTopicCate.getId());
        }
        if(lngxTopicCate.getName()!=null   && !lngxTopicCate.getName().trim().equals("")){
            q.like("name",lngxTopicCate.getName());
        }
        if(lngxTopicCate.getBeginCreateTime()!=null && lngxTopicCate.getEndCreateTime()!=null){
            q.between("create_time",lngxTopicCate.getBeginCreateTime(),lngxTopicCate.getEndCreateTime());
        }
        if(lngxTopicCate.getBeginCreateUid()!=null && lngxTopicCate.getEndCreateUid()!=null){
            q.between("create_uid",lngxTopicCate.getBeginCreateUid(),lngxTopicCate.getEndCreateUid());
        }
        if(lngxTopicCate.getBeginDeleteTime()!=null && lngxTopicCate.getEndDeleteTime()!=null){
            q.between("delete_time",lngxTopicCate.getBeginDeleteTime(),lngxTopicCate.getEndDeleteTime());
        }
        if(lngxTopicCate.getIsTop()!=null  ){
            q.eq("is_top",lngxTopicCate.getIsTop());
        }
        List<TopicCate> list = this.list(q);
        return list;
    }


}
