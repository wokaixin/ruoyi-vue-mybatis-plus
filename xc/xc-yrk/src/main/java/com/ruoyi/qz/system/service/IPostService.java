package com.ruoyi.qz.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.qz.system.domain.Post;
import com.ruoyi.qz.system.dto.PostDto;
/**
 * 动态Service接口
 * 
 * @author yichen
 * @date 2021-10-17
 */
public interface IPostService extends IService<Post>
{


    /**
     * 查询动态列表
     * 
     * @param lngxPost 动态
     * @return 动态集合
     */
    public List<Post> selectdList(PostDto lngxPost);

}
