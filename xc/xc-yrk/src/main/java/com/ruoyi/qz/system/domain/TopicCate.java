package com.ruoyi.qz.system.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.domain.XBaseEntity;

/**
 * 话题分类对象 lngx_topic_cate
 * 
 * @author yichen
 * @date 2021-10-17
 */
@Data
//@TableName("yrk_topic_cate")
public class TopicCate extends  XBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 分类id */
    @Excel(name = "分类id")
    private Long id;
    /** 分类名 */
    @Excel(name = "分类名")
    private String name;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 创建人uid */
    @Excel(name = "创建人uid")
    private Long createUid;
    /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deleteTime;
    /** 推荐类型 */
    @Excel(name = "推荐类型")
    private Long isTop;
    /** 图标 */
    @Excel(name = "图标")
    private String coverImage;
}
