package com.ruoyi.qz.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.qz.system.domain.User;
import com.ruoyi.qz.system.dto.UserDto;
/**
 * 用户Service接口
 * 
 * @author yichen
 * @date 2021-10-17
 */
public interface IUserService extends IService<User>
{


    /**
     * 查询用户列表
     * 
     * @param lngxUser 用户
     * @return 用户集合
     */
    public List<User> selectdList(UserDto lngxUser);

}
