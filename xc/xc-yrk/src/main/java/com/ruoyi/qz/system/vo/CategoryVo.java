package com.ruoyi.qz.system.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.vo.XVoBaseEntity;

/**
 * 圈子分类对象 lngx_category
 * 
 * @author yichen
 * @date 2021-10-17
 */
@Data
public class CategoryVo extends  XVoBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 分类id */
    @Excel(name = "分类id")
    private Long cateId;
    /** 分类名 */
    @Excel(name = "分类名")
    private String cateName;
    /** 是否推荐   1推荐  */
    @Excel(name = "是否推荐   1推荐 ")
    private Integer isTop;
    /** 图标 */
    @Excel(name = "图标")
    private String coverImage;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deleteTime;
    /** 创建人uid */
    @Excel(name = "创建人uid")
    private Long createUid;
    /** 删除人uid */
    @Excel(name = "删除人uid")
    private Long deleteUid;
    /** 更新人uid */
    @Excel(name = "更新人uid")
    private Long updateUid;
    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
