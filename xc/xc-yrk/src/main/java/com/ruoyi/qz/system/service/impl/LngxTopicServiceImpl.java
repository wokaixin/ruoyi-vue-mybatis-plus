package com.ruoyi.qz.system.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.qz.system.mapper.TopicMapper;
import com.ruoyi.qz.system.domain.Topic;
import com.ruoyi.qz.system.dto.TopicDto;
import com.ruoyi.qz.system.service.ITopicService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 * 话题Service业务层处理
 *
 * @author yichen
 * @date 2021-10-17
 */
@Service
public class LngxTopicServiceImpl extends ServiceImpl<TopicMapper, Topic> implements ITopicService {
    @Autowired
    private TopicMapper lngxTopicMapper;

    /**
     * 查询话题列表
     * 
     * @param lngxTopic 话题
     * @return 话题
     */
    @Override
    public List<Topic> selectdList(TopicDto lngxTopic)
    {
        QueryWrapper<Topic> q= new QueryWrapper<>();
        if(lngxTopic.getId()!=null  ){
            q.eq("id",lngxTopic.getId());
        }
        if(lngxTopic.getUid()!=null  ){
            q.eq("uid",lngxTopic.getUid());
        }
        if(lngxTopic.getCateId()!=null  ){
            q.eq("cate_id",lngxTopic.getCateId());
        }
        if(lngxTopic.getTopicName()!=null   && !lngxTopic.getTopicName().trim().equals("")){
            q.like("topic_name",lngxTopic.getTopicName());
        }
        if(lngxTopic.getTopType()!=null  ){
            q.eq("top_type",lngxTopic.getTopType());
        }
        if(lngxTopic.getBeginCreateTime()!=null && lngxTopic.getEndCreateTime()!=null){
            q.between("create_time",lngxTopic.getBeginCreateTime(),lngxTopic.getEndCreateTime());
        }
        if(lngxTopic.getBeginDeleteTime()!=null && lngxTopic.getEndDeleteTime()!=null){
            q.between("delete_time",lngxTopic.getBeginDeleteTime(),lngxTopic.getEndDeleteTime());
        }
        if(lngxTopic.getBeginUpdateTime()!=null && lngxTopic.getEndUpdateTime()!=null){
            q.between("update_time",lngxTopic.getBeginUpdateTime(),lngxTopic.getEndUpdateTime());
        }
        List<Topic> list = this.list(q);
        return list;
    }


}
