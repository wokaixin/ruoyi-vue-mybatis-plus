package com.ruoyi.qz.system.vo;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.vo.XVoBaseEntity;

/**
 * 动态对象 lngx_post
 * 
 * @author yichen
 * @date 2021-10-17
 */
@Data
public class PostVo extends  XVoBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 动态id */
    @Excel(name = "动态id")
    private Long id;
    /** 用户id */
    @Excel(name = "用户id")
    private Long uid;
    /** 圈子id */
    @Excel(name = "圈子id")
    private Long topicId;
    /** 话题id */
    @Excel(name = "话题id")
    private Long discussId;
    /** 标题 */
    @Excel(name = "标题")
    private String title;
    /** 内容 */
    private String content;
    /** 媒体 */
    @Excel(name = "媒体")
    private String media;
    /** 阅读数量 */
    @Excel(name = "阅读数量")
    private Long readCount;
    /** 置顶 */
    @Excel(name = "置顶")
    private Integer postTop;
    /** 帖子类型 */
    @Excel(name = "帖子类型")
    private Integer type;
    /** 地址名称 */
    @Excel(name = "地址名称")
    private String address;
    /** 经度 */
    @Excel(name = "经度")
    private BigDecimal longitude;
    /** 纬度 */
    @Excel(name = "纬度")
    private BigDecimal latitude;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deleteTime;
    /** 地区代码 */
    @Excel(name = "地区代码")
    private Long cityCode;
    /** 图片 */
    @Excel(name = "图片")
    private String image;
}
