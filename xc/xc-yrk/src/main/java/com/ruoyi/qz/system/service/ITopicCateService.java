package com.ruoyi.qz.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.qz.system.domain.TopicCate;
import com.ruoyi.qz.system.dto.TopicCateDto;
/**
 * 话题分类Service接口
 * 
 * @author yichen
 * @date 2021-10-17
 */
public interface ITopicCateService extends IService<TopicCate>
{


    /**
     * 查询话题分类列表
     * 
     * @param lngxTopicCate 话题分类
     * @return 话题分类集合
     */
    public List<TopicCate> selectdList(TopicCateDto lngxTopicCate);

}
