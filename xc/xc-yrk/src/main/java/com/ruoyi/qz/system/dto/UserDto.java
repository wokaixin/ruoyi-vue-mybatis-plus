package com.ruoyi.qz.system.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.xc.common.dto.XDtoBaseEntity;

/**
 * 用户对象 lngx_user
 * 
 * @author yichen
 * @date 2021-10-17
 */
@Data
public class UserDto extends  XDtoBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 用户id */
    private Long uid;
            /** 用户名 */
    private String username;
            /** 密码 */
    private String password;
            /** 昵称 */
    private String nickname;
            /** 用户组id */
    private Long groupId;
            /** 性别 */
    private Integer gender;
            /** 省 */
    private String province;
            /** 城市 */
    private String city;
            /** 开发平台id */
    private String openid;
            /** 状态 */
    private Integer status;
            /** 介绍 */
    private String intro;
            /** 积分 */
    private Long integral;
            /** websocket用户Id多个逗号分割 */
    private Long fd;
            /** 上次登陆IP */
    private String lastLoginIp;
            /** 用户标签 */
    private String tagStr;
            /** 0为普通用户，1为官方账号 */
    private Integer type;
            /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginCreateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endCreateTime;
        /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginDeleteTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDeleteTime;
        /** 用户wss链接ip:fd 多个逗号分割 */
    private String wss;
            /** 加密值 */
    private String secret;
            /** 开放平台id */
    private String unionid;
        


}
