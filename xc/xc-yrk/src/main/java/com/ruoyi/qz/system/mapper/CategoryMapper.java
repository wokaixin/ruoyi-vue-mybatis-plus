package com.ruoyi.qz.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.qz.system.domain.Category;

/**
 * 圈子分类Mapper接口
 * 
 * @author yichen
 * @date 2021-10-17
 */
public interface CategoryMapper extends BaseMapper<Category>
{



}
