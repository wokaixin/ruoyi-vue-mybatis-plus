package com.ruoyi.qz.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.qz.system.domain.User;

/**
 * 用户Mapper接口
 * 
 * @author yichen
 * @date 2021-10-17
 */
public interface UserMapper extends BaseMapper<User>
{



}
