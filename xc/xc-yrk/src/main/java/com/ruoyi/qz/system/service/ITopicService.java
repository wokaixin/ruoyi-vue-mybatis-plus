package com.ruoyi.qz.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.qz.system.domain.Topic;
import com.ruoyi.qz.system.dto.TopicDto;
/**
 * 话题Service接口
 * 
 * @author yichen
 * @date 2021-10-17
 */
public interface ITopicService extends IService<Topic>
{


    /**
     * 查询话题列表
     * 
     * @param lngxTopic 话题
     * @return 话题集合
     */
    public List<Topic> selectdList(TopicDto lngxTopic);

}
