package com.ruoyi.qz.system.controller;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.qz.system.domain.Topic;
import com.ruoyi.qz.system.dto.TopicDto;
import com.ruoyi.qz.system.service.ITopicService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;


/**
 * 话题Controller
 *
 * @author yichen
 * @date 2021-10-17
 */
@RestController
@RequestMapping("/topic")
public class TopicController extends BaseController
{
    @Autowired
    private ITopicService lngxTopicService;

/**
 * 查询话题列表
 */
@PreAuthorize("@ss.hasPermi('lngx:topic:list')")
@GetMapping("/list")
        public TableDataInfo list(TopicDto lngxTopic)
    {
        startPage();
        List<Topic> list = lngxTopicService.selectdList(lngxTopic);
        return getDataTable(list);
    }
    
    /**
     * 导出话题列表
     */
    @PreAuthorize("@ss.hasPermi('lngx:topic:export')")
    @Log(title = "话题", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TopicDto lngxTopic) throws IOException
    {

        startPage();
        List<Topic> list = lngxTopicService.selectdList(lngxTopic);
        ExcelUtil<Topic> util = new ExcelUtil<Topic>(Topic.class);
        util.exportExcel(response, list, "话题数据");
    }
    /**
     * 导入话题
     */
    @PreAuthorize("@ss.hasPermi('lngx:topic:importData')")
    @Log(title = "话题导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<Topic> util = new ExcelUtil<Topic>( Topic.class);
        List<Topic> list = util.importExcel(file.getInputStream());
        boolean message=false;
        if(updateSupport){
            message =lngxTopicService.saveOrUpdateBatch(list);
        }else {
            message =lngxTopicService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }
    /**
     * 获取话题详细信息
     */
    @PreAuthorize("@ss.hasPermi('lngx:topic:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(lngxTopicService.getById(id));
    }

    /**
     * 新增话题
     */
    @PreAuthorize("@ss.hasPermi('lngx:topic:add')")
    @Log(title = "话题", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Topic lngxTopic)
    {
        return toAjax(lngxTopicService.save(lngxTopic));
    }

    /**
     * 修改话题
     */
    @PreAuthorize("@ss.hasPermi('lngx:topic:edit')")
    @Log(title = "话题", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Topic lngxTopic)
    {
        return toAjax(lngxTopicService.updateById(lngxTopic));
    }

    /**
     * 删除话题
     */
    @PreAuthorize("@ss.hasPermi('lngx:topic:remove')")
    @Log(title = "话题", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids)
    {
        return toAjax(lngxTopicService.removeByIds(ids));
    }
}
