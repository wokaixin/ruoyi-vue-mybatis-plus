package com.ruoyi.qz.system.controller;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.qz.system.domain.TopicCate;
import com.ruoyi.qz.system.dto.TopicCateDto;
import com.ruoyi.qz.system.service.ITopicCateService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;


/**
 * 话题分类Controller
 *
 * @author yichen
 * @date 2021-10-17
 */
@RestController
@RequestMapping("/topic_cate")
public class TopicCateController extends BaseController
{
    @Autowired
    private ITopicCateService lngxTopicCateService;

/**
 * 查询话题分类列表
 */
@PreAuthorize("@ss.hasPermi('lngx:topic_cate:list')")
@GetMapping("/list")
        public TableDataInfo list(TopicCateDto lngxTopicCate)
    {
        startPage();
        List<TopicCate> list = lngxTopicCateService.selectdList(lngxTopicCate);
        return getDataTable(list);
    }
    
    /**
     * 导出话题分类列表
     */
    @PreAuthorize("@ss.hasPermi('lngx:topic_cate:export')")
    @Log(title = "话题分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TopicCateDto lngxTopicCate) throws IOException
    {

        startPage();
        List<TopicCate> list = lngxTopicCateService.selectdList(lngxTopicCate);
        ExcelUtil<TopicCate> util = new ExcelUtil<TopicCate>(TopicCate.class);
        util.exportExcel(response, list, "话题分类数据");
    }
    /**
     * 导入话题分类
     */
    @PreAuthorize("@ss.hasPermi('lngx:topic_cate:importData')")
    @Log(title = "话题分类导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<TopicCate> util = new ExcelUtil<TopicCate>( TopicCate.class);
        List<TopicCate> list = util.importExcel(file.getInputStream());
        boolean message=false;
        if(updateSupport){
            message =lngxTopicCateService.saveOrUpdateBatch(list);
        }else {
            message =lngxTopicCateService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }
    /**
     * 获取话题分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('lngx:topic_cate:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(lngxTopicCateService.getById(id));
    }

    /**
     * 新增话题分类
     */
    @PreAuthorize("@ss.hasPermi('lngx:topic_cate:add')")
    @Log(title = "话题分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TopicCate lngxTopicCate)
    {
        return toAjax(lngxTopicCateService.save(lngxTopicCate));
    }

    /**
     * 修改话题分类
     */
    @PreAuthorize("@ss.hasPermi('lngx:topic_cate:edit')")
    @Log(title = "话题分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TopicCate lngxTopicCate)
    {
        return toAjax(lngxTopicCateService.updateById(lngxTopicCate));
    }

    /**
     * 删除话题分类
     */
    @PreAuthorize("@ss.hasPermi('lngx:topic_cate:remove')")
    @Log(title = "话题分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids)
    {
        return toAjax(lngxTopicCateService.removeByIds(ids));
    }
}
