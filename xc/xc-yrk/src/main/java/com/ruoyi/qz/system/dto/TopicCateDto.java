package com.ruoyi.qz.system.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.xc.common.dto.XDtoBaseEntity;

/**
 * 话题分类对象 lngx_topic_cate
 * 
 * @author yichen
 * @date 2021-10-17
 */
@Data
public class TopicCateDto extends  XDtoBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 分类id */
    private Long id;
            /** 分类名 */
    private String name;
            /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginCreateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endCreateTime;
        /** 创建人uid */
    private Long beginCreateUid;
    private Long endCreateUid;
        /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginDeleteTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDeleteTime;
        /** 推荐类型 */
    private Long isTop;
        


}
