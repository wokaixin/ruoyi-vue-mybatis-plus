package com.ruoyi.qz.system.controller;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.qz.system.domain.Category;
import com.ruoyi.qz.system.dto.CategoryDto;
import com.ruoyi.qz.system.service.ICategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;


/**
 * 圈子分类Controller
 *
 * @author yichen
 * @date 2021-10-17
 */
@RestController
@RequestMapping("/category")
public class CategoryController extends BaseController
{
    @Autowired
    private ICategoryService lngxCategoryService;

/**
 * 查询圈子分类列表
 */
@PreAuthorize("@ss.hasPermi('lngx:category:list')")
@GetMapping("/list")
        public TableDataInfo list(CategoryDto lngxCategory)
    {
        startPage();
        List<Category> list = lngxCategoryService.selectdList(lngxCategory);
        return getDataTable(list);
    }
    
    /**
     * 导出圈子分类列表
     */
    @PreAuthorize("@ss.hasPermi('lngx:category:export')")
    @Log(title = "圈子分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CategoryDto lngxCategory) throws IOException
    {

        startPage();
        List<Category> list = lngxCategoryService.selectdList(lngxCategory);
        ExcelUtil<Category> util = new ExcelUtil<Category>(Category.class);
        util.exportExcel(response, list, "圈子分类数据");
    }
    /**
     * 导入圈子分类
     */
    @PreAuthorize("@ss.hasPermi('lngx:category:importData')")
    @Log(title = "圈子分类导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<Category> util = new ExcelUtil<Category>( Category.class);
        List<Category> list = util.importExcel(file.getInputStream());
        boolean message=false;
        if(updateSupport){
            message =lngxCategoryService.saveOrUpdateBatch(list);
        }else {
            message =lngxCategoryService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }
    /**
     * 获取圈子分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('lngx:category:query')")
    @GetMapping(value = "/{cateId}")
    public AjaxResult getInfo(@PathVariable("cateId") Long cateId)
    {
        return AjaxResult.success(lngxCategoryService.getById(cateId));
    }

    /**
     * 新增圈子分类
     */
    @PreAuthorize("@ss.hasPermi('lngx:category:add')")
    @Log(title = "圈子分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Category category)
    {
        return toAjax(lngxCategoryService.save(category));
    }

    /**
     * 修改圈子分类
     */
    @PreAuthorize("@ss.hasPermi('lngx:category:edit')")
    @Log(title = "圈子分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Category category)
    {
        return toAjax(lngxCategoryService.updateById(category));
    }

    /**
     * 删除圈子分类
     */
    @PreAuthorize("@ss.hasPermi('lngx:category:remove')")
    @Log(title = "圈子分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{cateIds}")
    public AjaxResult remove(@PathVariable List<Long> cateIds)
    {
        return toAjax(lngxCategoryService.removeByIds(cateIds));
    }
}
