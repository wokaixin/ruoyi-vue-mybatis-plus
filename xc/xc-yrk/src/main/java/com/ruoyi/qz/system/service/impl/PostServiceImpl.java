package com.ruoyi.qz.system.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.qz.system.mapper.PostMapper;
import com.ruoyi.qz.system.domain.Post;
import com.ruoyi.qz.system.dto.PostDto;
import com.ruoyi.qz.system.service.IPostService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 * 动态Service业务层处理
 *
 * @author yichen
 * @date 2021-10-17
 */
@Service
public class PostServiceImpl extends ServiceImpl<PostMapper, Post> implements IPostService {
    @Autowired
    private PostMapper lngxPostMapper;

    /**
     * 查询动态列表
     * 
     * @param lngxPost 动态
     * @return 动态
     */
    @Override
    public List<Post> selectdList(PostDto lngxPost)
    {
        QueryWrapper<Post> q= new QueryWrapper<>();
        if(lngxPost.getId()!=null  ){
            q.eq("id",lngxPost.getId());
        }
        if(lngxPost.getUid()!=null  ){
            q.eq("uid",lngxPost.getUid());
        }
        if(lngxPost.getTopicId()!=null  ){
            q.eq("topic_id",lngxPost.getTopicId());
        }
        if(lngxPost.getDiscussId()!=null  ){
            q.eq("discuss_id",lngxPost.getDiscussId());
        }
        if(lngxPost.getTitle()!=null   && !lngxPost.getTitle().trim().equals("")){
            q.like("title",lngxPost.getTitle());
        }
        if(lngxPost.getBeginReadCount()!=null && lngxPost.getEndReadCount()!=null){
            q.between("read_count",lngxPost.getBeginReadCount(),lngxPost.getEndReadCount());
        }
        if(lngxPost.getPostTop()!=null  ){
            q.eq("post_top",lngxPost.getPostTop());
        }
        if(lngxPost.getType()!=null  ){
            q.eq("type",lngxPost.getType());
        }
        if(lngxPost.getAddress()!=null   && !lngxPost.getAddress().trim().equals("")){
            q.like("address",lngxPost.getAddress());
        }
        if(lngxPost.getBeginLongitude()!=null && lngxPost.getEndLongitude()!=null){
            q.between("longitude",lngxPost.getBeginLongitude(),lngxPost.getEndLongitude());
        }
        if(lngxPost.getBeginLatitude()!=null && lngxPost.getEndLatitude()!=null){
            q.between("latitude",lngxPost.getBeginLatitude(),lngxPost.getEndLatitude());
        }
        if(lngxPost.getBeginCreateTime()!=null && lngxPost.getEndCreateTime()!=null){
            q.between("create_time",lngxPost.getBeginCreateTime(),lngxPost.getEndCreateTime());
        }
        if(lngxPost.getBeginDeleteTime()!=null && lngxPost.getEndDeleteTime()!=null){
            q.between("delete_time",lngxPost.getBeginDeleteTime(),lngxPost.getEndDeleteTime());
        }
        if(lngxPost.getCityCode()!=null  ){
            q.eq("city_code",lngxPost.getCityCode());
        }
        List<Post> list = this.list(q);
        return list;
    }


}
