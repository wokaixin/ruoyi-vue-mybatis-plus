package com.ruoyi.qz.system.controller;

import java.io.File;
import java.util.Date;
import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import com.xc.common.utils.FilezUtils;
import com.xc.common.utils.WordUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

import com.ruoyi.qz.system.domain.Post;
import com.ruoyi.qz.system.dto.PostDto;
import com.ruoyi.qz.system.service.IPostService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;


/**
 * 动态Controller
 *
 * @author yichen
 * @date 2021-10-17
 */
@RestController
@RequestMapping("/post")
public class PostController extends BaseController {
    @Autowired
    private IPostService lngxPostService;

    /**
     * 查询动态列表
     */
    @PreAuthorize("@ss.hasPermi('lngx:post:list')")
    @GetMapping("/list")
    public TableDataInfo list(PostDto lngxPost) {
        startPage();
        List<Post> list = lngxPostService.selectdList(lngxPost);
        return getDataTable(list);
    }

    /**
     * 导出动态列表
     */
    @PreAuthorize("@ss.hasPermi('lngx:post:export')")
    @Log(title = "动态", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PostDto lngxPost) throws IOException {

        startPage();
        List<Post> list = lngxPostService.selectdList(lngxPost);
        ExcelUtil<Post> util = new ExcelUtil<Post>(Post.class);
        util.exportExcel(response, list, "动态数据");
    }

    /**
     * 导入动态
     */
    @PreAuthorize("@ss.hasPermi('lngx:post:importData')")
    @Log(title = "动态导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<Post> util = new ExcelUtil<Post>(Post.class);
        List<Post> list = util.importExcel(file.getInputStream());
        Long uid=SecurityUtils.getUserId();
        for (Post item:list) {
            item.setUid(uid);
        }
        boolean message = false;
        if (updateSupport) {
            message = lngxPostService.saveOrUpdateBatch(list);
        } else {
            message = lngxPostService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 导入文档转文字
     */
    @PreAuthorize("@ss.hasPermi('lngx:post:importDoc')")
    @Log(title = "doc文档转文字导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importDoc")
    public AjaxResult importDoc(MultipartFile file, boolean updateSupport) throws Exception {
        Post post =new Post();
        String str = "";
        boolean message = false;
        File filez = null;
        try {
            // 获取文件名
            filez= FilezUtils.transferToFile(file);
            str= WordUtils.toString(filez);
            return AjaxResult.success(str);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return AjaxResult.error("文件解析失败");
    }
    /**
     * 获取动态详细信息
     */
    @PreAuthorize("@ss.hasPermi('lngx:post:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(lngxPostService.getById(id));
    }

    /**
     * 新增动态
     */
    @PreAuthorize("@ss.hasPermi('lngx:post:add')")
    @Log(title = "动态", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Post lngxPost) {
        lngxPost.setUid(SecurityUtils.getUserId());
        return toAjax(lngxPostService.save(lngxPost));
    }

    /**
     * 修改动态
     */
    @PreAuthorize("@ss.hasPermi('lngx:post:edit')")
    @Log(title = "动态", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Post lngxPost) {
        lngxPost.setUid(SecurityUtils.getUserId());
        return toAjax(lngxPostService.updateById(lngxPost));
    }

    /**
     * 删除动态
     * isDel 是否强制删除
     */
    @PreAuthorize("@ss.hasPermi('lngx:post:remove')")
    @Log(title = "动态", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids,Boolean isDel) {
        if(isDel!=null){
            return toAjax(lngxPostService.removeByIds(ids));
        }
        Integer r=0;
//       Long uid= SecurityUtils.getUserId();
        for (Long id:ids) {
            Post lngxPost= lngxPostService.getById (id);
//            if(lngxPost!=null && lngxPost.getUid()==uid){
//
//            }
            if(lngxPost!=null){
                lngxPost.setDeleteTime(new Date());
             if(lngxPostService.updateById(lngxPost)){
                 r++;
             }
            }
        }
        return AjaxResult.success("成功删除"+r.toString()+"条数据");

    }
}
