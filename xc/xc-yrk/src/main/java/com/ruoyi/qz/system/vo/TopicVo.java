package com.ruoyi.qz.system.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.annotation.Excel;
import com.xc.common.vo.XVoBaseEntity;

/**
 * 话题对象 lngx_topic
 * 
 * @author yichen
 * @date 2021-10-17
 */
@Data
public class TopicVo extends  XVoBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 主键id */
    private Long id;
    /** 用户id */
    @Excel(name = "用户id")
    private Long uid;
    /** 分类id */
    @Excel(name = "分类id")
    private Long cateId;
    /** 话题名称 */
    @Excel(name = "话题名称")
    private String topicName;
    /** 描述 */
    @Excel(name = "描述")
    private String description;
    /** 图标 */
    @Excel(name = "图标")
    private String coverImage;
    /** 背景图片 */
    @Excel(name = "背景图片")
    private String bgImage;
    /** 推荐类型：0 不推荐 1首页推荐 */
    @Excel(name = "推荐类型：0 不推荐 1首页推荐")
    private Integer topType;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deleteTime;
    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
