package com.ruoyi.qz.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.qz.system.domain.Category;
import com.ruoyi.qz.system.dto.CategoryDto;
/**
 * 圈子分类Service接口
 * 
 * @author yichen
 * @date 2021-10-17
 */
public interface ICategoryService extends IService<Category>
{


    /**
     * 查询圈子分类列表
     * 
     * @param lngxCategory 圈子分类
     * @return 圈子分类集合
     */
    public List<Category> selectdList(CategoryDto lngxCategory);

}
