package com.ruoyi.qz.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.qz.system.domain.Post;

/**
 * 动态Mapper接口
 * 
 * @author yichen
 * @date 2021-10-17
 */
public interface PostMapper extends BaseMapper<Post>
{



}
