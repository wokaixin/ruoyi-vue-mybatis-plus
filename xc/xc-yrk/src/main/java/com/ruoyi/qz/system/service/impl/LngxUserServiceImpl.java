package com.ruoyi.qz.system.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.qz.system.mapper.UserMapper;
import com.ruoyi.qz.system.domain.User;
import com.ruoyi.qz.system.dto.UserDto;
import com.ruoyi.qz.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 * 用户Service业务层处理
 *
 * @author yichen
 * @date 2021-10-17
 */
@Service
public class LngxUserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Autowired
    private UserMapper lngxUserMapper;

    /**
     * 查询用户列表
     * 
     * @param lngxUser 用户
     * @return 用户
     */
    @Override
    public List<User> selectdList(UserDto lngxUser)
    {
        QueryWrapper<User> q= new QueryWrapper<>();
        if(lngxUser.getUid()!=null  ){
            q.eq("uid",lngxUser.getUid());
        }
        if(lngxUser.getUsername()!=null   && !lngxUser.getUsername().trim().equals("")){
            q.like("username",lngxUser.getUsername());
        }
        if(lngxUser.getPassword()!=null   && !lngxUser.getPassword().trim().equals("")){
            q.eq("password",lngxUser.getPassword());
        }
        if(lngxUser.getNickname()!=null   && !lngxUser.getNickname().trim().equals("")){
            q.like("nickname",lngxUser.getNickname());
        }
        if(lngxUser.getGroupId()!=null  ){
            q.eq("group_id",lngxUser.getGroupId());
        }
        if(lngxUser.getGender()!=null  ){
            q.eq("gender",lngxUser.getGender());
        }
        if(lngxUser.getProvince()!=null   && !lngxUser.getProvince().trim().equals("")){
            q.eq("province",lngxUser.getProvince());
        }
        if(lngxUser.getCity()!=null   && !lngxUser.getCity().trim().equals("")){
            q.eq("city",lngxUser.getCity());
        }
        if(lngxUser.getOpenid()!=null   && !lngxUser.getOpenid().trim().equals("")){
            q.eq("openid",lngxUser.getOpenid());
        }
        if(lngxUser.getStatus()!=null  ){
            q.eq("status",lngxUser.getStatus());
        }
        if(lngxUser.getIntro()!=null   && !lngxUser.getIntro().trim().equals("")){
            q.eq("intro",lngxUser.getIntro());
        }
        if(lngxUser.getIntegral()!=null  ){
            q.eq("integral",lngxUser.getIntegral());
        }
        if(lngxUser.getFd()!=null  ){
            q.eq("fd",lngxUser.getFd());
        }
        if(lngxUser.getLastLoginIp()!=null   && !lngxUser.getLastLoginIp().trim().equals("")){
            q.eq("last_login_ip",lngxUser.getLastLoginIp());
        }
        if(lngxUser.getTagStr()!=null   && !lngxUser.getTagStr().trim().equals("")){
            q.eq("tag_str",lngxUser.getTagStr());
        }
        if(lngxUser.getType()!=null  ){
            q.eq("type",lngxUser.getType());
        }
        if(lngxUser.getBeginCreateTime()!=null && lngxUser.getEndCreateTime()!=null){
            q.between("create_time",lngxUser.getBeginCreateTime(),lngxUser.getEndCreateTime());
        }
        if(lngxUser.getBeginDeleteTime()!=null && lngxUser.getEndDeleteTime()!=null){
            q.between("delete_time",lngxUser.getBeginDeleteTime(),lngxUser.getEndDeleteTime());
        }
        if(lngxUser.getWss()!=null   && !lngxUser.getWss().trim().equals("")){
            q.eq("wss",lngxUser.getWss());
        }
        if(lngxUser.getSecret()!=null   && !lngxUser.getSecret().trim().equals("")){
            q.eq("secret",lngxUser.getSecret());
        }
        if(lngxUser.getUnionid()!=null   && !lngxUser.getUnionid().trim().equals("")){
            q.eq("unionid",lngxUser.getUnionid());
        }
        List<User> list = this.list(q);
        return list;
    }


}
