package com.ruoyi.qz.system.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.qz.system.mapper.CategoryMapper;
import com.ruoyi.qz.system.domain.Category;
import com.ruoyi.qz.system.dto.CategoryDto;
import com.ruoyi.qz.system.service.ICategoryService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 * 圈子分类Service业务层处理
 *
 * @author yichen
 * @date 2021-10-17
 */
@Service
public class LngxCategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {
    @Autowired
    private CategoryMapper lngxCategoryMapper;

    /**
     * 查询圈子分类列表
     * 
     * @param lngxCategory 圈子分类
     * @return 圈子分类
     */
    @Override
    public List<Category> selectdList(CategoryDto lngxCategory)
    {
        QueryWrapper<Category> q= new QueryWrapper<>();
        if(lngxCategory.getCateId()!=null  ){
            q.eq("cate_id",lngxCategory.getCateId());
        }
        if(lngxCategory.getCateName()!=null   && !lngxCategory.getCateName().trim().equals("")){
            q.like("cate_name",lngxCategory.getCateName());
        }
        if(lngxCategory.getIsTop()!=null  ){
            q.eq("is_top",lngxCategory.getIsTop());
        }
        if(lngxCategory.getBeginCreateTime()!=null && lngxCategory.getEndCreateTime()!=null){
            q.between("create_time",lngxCategory.getBeginCreateTime(),lngxCategory.getEndCreateTime());
        }
        if(lngxCategory.getBeginDeleteTime()!=null && lngxCategory.getEndDeleteTime()!=null){
            q.between("delete_time",lngxCategory.getBeginDeleteTime(),lngxCategory.getEndDeleteTime());
        }
        if(lngxCategory.getCreateUid()!=null  ){
            q.eq("create_uid",lngxCategory.getCreateUid());
        }
        if(lngxCategory.getDeleteUid()!=null  ){
            q.eq("delete_uid",lngxCategory.getDeleteUid());
        }
        if(lngxCategory.getUpdateUid()!=null  ){
            q.eq("update_uid",lngxCategory.getUpdateUid());
        }
        if(lngxCategory.getBeginUpdateTime()!=null && lngxCategory.getEndUpdateTime()!=null){
            q.between("update_time",lngxCategory.getBeginUpdateTime(),lngxCategory.getEndUpdateTime());
        }
        List<Category> list = this.list(q);
        return list;
    }


}
