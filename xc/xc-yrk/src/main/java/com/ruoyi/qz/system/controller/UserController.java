package com.ruoyi.qz.system.controller;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.qz.system.domain.User;
import com.ruoyi.qz.system.dto.UserDto;
import com.ruoyi.qz.system.service.IUserService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;


/**
 * 用户Controller
 *
 * @author yichen
 * @date 2021-10-17
 */
@RestController
@RequestMapping("/user")
public class UserController extends BaseController
{
    @Autowired
    private IUserService lngxUserService;

/**
 * 查询用户列表
 */
@PreAuthorize("@ss.hasPermi('lngx:user:list')")
@GetMapping("/list")
        public TableDataInfo list(UserDto lngxUser)
    {
        startPage();
        List<User> list = lngxUserService.selectdList(lngxUser);
        return getDataTable(list);
    }
    
    /**
     * 导出用户列表
     */
    @PreAuthorize("@ss.hasPermi('lngx:user:export')")
    @Log(title = "用户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UserDto lngxUser) throws IOException
    {

        startPage();
        List<User> list = lngxUserService.selectdList(lngxUser);
        ExcelUtil<User> util = new ExcelUtil<User>(User.class);
        util.exportExcel(response, list, "用户数据");
    }
    /**
     * 导入用户
     */
    @PreAuthorize("@ss.hasPermi('lngx:user:importData')")
    @Log(title = "用户导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<User> util = new ExcelUtil<User>( User.class);
        List<User> list = util.importExcel(file.getInputStream());
        boolean message=false;
        if(updateSupport){
            message =lngxUserService.saveOrUpdateBatch(list);
        }else {
            message =lngxUserService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }
    /**
     * 获取用户详细信息
     */
    @PreAuthorize("@ss.hasPermi('lngx:user:query')")
    @GetMapping(value = "/{uid}")
    public AjaxResult getInfo(@PathVariable("uid") Long uid)
    {
        return AjaxResult.success(lngxUserService.getById(uid));
    }

    /**
     * 新增用户
     */
    @PreAuthorize("@ss.hasPermi('lngx:user:add')")
    @Log(title = "用户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody User user)
    {
        return toAjax(lngxUserService.save(user));
    }

    /**
     * 修改用户
     */
    @PreAuthorize("@ss.hasPermi('lngx:user:edit')")
    @Log(title = "用户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody User user)
    {
        return toAjax(lngxUserService.updateById(user));
    }

    /**
     * 删除用户
     */
    @PreAuthorize("@ss.hasPermi('lngx:user:remove')")
    @Log(title = "用户", businessType = BusinessType.DELETE)
    @DeleteMapping("/{uids}")
    public AjaxResult remove(@PathVariable List<Long> uids)
    {
        return toAjax(lngxUserService.removeByIds(uids));
    }
}
