package com.ruoyi.qz.system.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.xc.common.dto.XDtoBaseEntity;

/**
 * 圈子分类对象 lngx_category
 * 
 * @author yichen
 * @date 2021-10-17
 */
@Data
public class CategoryDto extends  XDtoBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 分类id */
    private Long cateId;
            /** 分类名 */
    private String cateName;
            /** 是否推荐   1推荐  */
    private Integer isTop;
            /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginCreateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endCreateTime;
        /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginDeleteTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDeleteTime;
        /** 创建人uid */
    private Long createUid;
            /** 删除人uid */
    private Long deleteUid;
            /** 更新人uid */
    private Long updateUid;
            /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginUpdateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endUpdateTime;
    


}
