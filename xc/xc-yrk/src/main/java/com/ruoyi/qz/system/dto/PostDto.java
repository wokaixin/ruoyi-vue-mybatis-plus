package com.ruoyi.qz.system.dto;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.xc.common.dto.XDtoBaseEntity;

/**
 * 动态对象 lngx_post
 * 
 * @author yichen
 * @date 2021-10-17
 */
@Data
public class PostDto extends  XDtoBaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 动态id */
    private Long id;
            /** 用户id */
    private Long uid;
            /** 圈子id */
    private Long topicId;
            /** 话题id */
    private Long discussId;
            /** 标题 */
    private String title;
            /** 阅读数量 */
    private Long beginReadCount;
    private Long endReadCount;
        /** 置顶 */
    private Integer postTop;
            /** 帖子类型 */
    private Integer type;
            /** 地址名称 */
    private String address;
            /** 经度 */
    private BigDecimal beginLongitude;
    private BigDecimal endLongitude;
        /** 纬度 */
    private BigDecimal beginLatitude;
    private BigDecimal endLatitude;
        /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginCreateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endCreateTime;
        /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginDeleteTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDeleteTime;
        /** 地区代码 */
    private Long cityCode;
        


}
