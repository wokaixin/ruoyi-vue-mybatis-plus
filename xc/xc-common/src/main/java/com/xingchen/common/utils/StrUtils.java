package com.xingchen.common.utils;

import com.ruoyi.common.utils.StringUtils;

import javax.validation.constraints.NotNull;
import java.io.UnsupportedEncodingException;

//java 非法字符过滤 , 半角/全角替换
public class StrUtils {
    //中文标点符号
    final static String[] chinesePunctuation;


    //英文标点符号
    final static String[] englishPunctuation;

    static {
        chinesePunctuation = new String[]{"！", "，", "。", "；", "《", "》", "（", "）", "？","｛", "｝", "：", "【", "】", "”", "‘"};
        englishPunctuation = new String[]{"\\!", ",", "\\.", ";", "<", ">", "\\(", "\\)", "\\?", "\\{", "\\}",  "\\:", "\\[", "\\]", "\"", "\'"};
    }
    public static final String delHuanhang(String str){
       return str.replace("\t","").replace("\r","").replace("\n","");
    }
    public  static  final  String delHuanhangAndKong(String str){
        return  delHuanhang(str).replace("\\s","");
    }
    public static final String toEnglish(String str) {
        return toEnglish(str,true);
    }
    /**
     * 中文标点符号转英文字标点符号
     *
     * @param str 原字符串
     * @return str 新字符串
     */
    public static final String toEnglish(String str,Boolean isToEnglish) {
        if (StringUtils.isEmpty(str)) {
            return str;
        }
        if(isToEnglish){
            //去除空格
//            str = str.replaceAll("\\s+", "");
            for (int i = 0; i < chinesePunctuation.length; i++) {
                str = str.replaceAll(chinesePunctuation[i], englishPunctuation[i]);
            }
            return str;
        }
        //去除空格
//        str = str.replaceAll("\\s+", "");
        for (int i = 0; i < englishPunctuation.length; i++) {
//            System.out.println(i);
            str = str.replaceAll(englishPunctuation[i],chinesePunctuation[i]);
        }
        return str;
    }
    /**
     * 去掉回车、空格、制表符
     *
     * @param str 原字符串
     * @return 转换后的字符串
     */
    public static String trimAll(String str){
        if (str == null){
            return null;
        }
        if (str.length() == 0){
            return str;
        }
        // 去掉空格
        str = str.trim();
        // 去掉回车
        str = trimR(str);
        // 去掉换行符
        str = trimN(str);
        // 去掉制表符
        str = trimT(str);
        return str;
    }

    /**
     * 去掉字符串左右回车
     *
     * @param str 原字符串
     * @return 转换后的字符串
     */
    public static String trimR(@NotNull String str){
        int len = str.length();
        int st = 0;
        char[] val = str.toCharArray();

        while ((st < len) && (val[st] <= '\n')) {
            st++;
        }
        while ((st < len) && (val[len - 1] <= '\n')) {
            len--;
        }
        return ((st > 0) || (len < str.length())) ? str.substring(st, len) : str;
    }

    /**
     * 去掉字符串左右换行
     *
     * @param str 原字符串
     * @return 转换后的字符串
     */
    public static String trimN(@NotNull String str){
        int len = str.length();
        int st = 0;
        char[] val = str.toCharArray();

        while ((st < len) && (val[st] <= '\r')) {
            st++;
        }
        while ((st < len) && (val[len - 1] <= '\r')) {
            len--;
        }
        return ((st > 0) || (len < str.length())) ? str.substring(st, len) : str;
    }

    /**
     * 去掉字符串左右制表符
     *
     * @param str 原字符串
     * @return 转换后的字符串
     */
    public static String trimT(@NotNull String str){
        int len = str.length();
        int st = 0;
        char[] val = str.toCharArray();

        while ((st < len) && (val[st] <= '\t')) {
            st++;
        }
        while ((st < len) && (val[len - 1] <= '\t')) {
            len--;
        }
        return ((st > 0) || (len < str.length())) ? str.substring(st, len) : str;
    }
//    // " “  ""
//    public static void main(String[] args) {
//        // System.out.println(full2Half("你好"));
//        // System.out.println(full2Half("ｊａｖａ"));
//        // System.out.println("中文:" + full2Half("你好"));
//        String s = " “中，。,. 国”，国‘家’。5：: ： ；；；；4  【】）";
//        System.out.println(s);
//        System.out.println(cToe(s));
//    }

    /**
     * 判断字符串是否为空或空字符串
     *
     * @param str 原字符串
     * @return
     */
    public static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }

    /**
     * 全角转半角:
     *
     * @param fullStr
     * @return
     */
    public static final String full2Half(String fullStr) {
        if (isEmpty(fullStr)) {
            return fullStr;
        }
        char[] c = fullStr.toCharArray();
        for (int i = 0; i < c.length; i++) {
//            System.out.println((int) c[i]);
            if (c[i] >= 65281 && c[i] <= 65374) {
                c[i] = (char) (c[i] - 65248);
            } else if (c[i] == 12288) { // 空格
                c[i] = (char) 32;
            }
        }
        return new String(c);
    }

    /**
     * 半角转全角
     *
     * @param halfStr
     * @return
     */
    public static final String half2Full(String halfStr) {
        if (isEmpty(halfStr)) {
            return halfStr;
        }
        char[] c = halfStr.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (c[i] == 32) {
                c[i] = (char) 12288;
            } else if (c[i] < 127) {
                c[i] = (char) (c[i] + 65248);
            }
        }
        return new String(c);
    }

}