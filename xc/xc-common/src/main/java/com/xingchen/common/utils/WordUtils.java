package com.xc.common.utils;

import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.*;
import java.io.File;
import java.io.FileInputStream;

public class WordUtils{
        public static String toString(File filePath) {
            String text = "";

            String fileName = filePath.getName().toLowerCase();// 得到名字小写
            try {
                FileInputStream in = new FileInputStream(filePath);
                if (fileName.endsWith(".doc")) { // doc为后缀的

                    WordExtractor extractor = new WordExtractor(in);
                    text = extractor.getText();
                }else if (fileName.endsWith(".docx")) { // docx为后缀的

                    XWPFWordExtractor docx = new XWPFWordExtractor(new XWPFDocument(in));
                    text = docx.getText();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return text;
        }
}
